package com.example.tripark.data.repository

import com.example.tripark.data.api.ApiService

class ParkLocationRepository(private val apiService: ApiService) {
    suspend fun getLocations() = apiService.getLocations()
}