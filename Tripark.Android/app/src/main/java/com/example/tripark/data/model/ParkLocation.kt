package com.example.tripark.data.model

data class ParkLocation(
    var lat: String = "0",
    var lng: String = "0"
) {
}