package com.example.tripark.data.api

import com.example.tripark.data.model.ParkLocation
import retrofit2.Response

interface ApiHelper {
    suspend fun getLocations(): Response<ParkLocation>
}