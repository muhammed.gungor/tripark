package com.example.tripark.data.api

import com.example.tripark.data.model.ParkLocation
import retrofit2.Response
import retrofit2.http.POST

interface ApiService {
    @POST("locations")
    suspend fun getLocations(): Response<ParkLocation>
}