package com.example.tripark.ui.main.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.tripark.R
import com.example.tripark.databinding.FragmentQrBinding
import com.example.tripark.ui.main.view.activity.QRActivity
import com.google.zxing.integration.android.IntentIntegrator

class QRFragment : Fragment() {
    private lateinit var binding: FragmentQrBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentQrBinding.inflate(inflater, container, false)
        initView()
        return binding.root
    }

    private fun initView() {
        binding.qrBtn.setOnClickListener { openQR() }    }

    private fun openQR() {
        val intentIntegrator = IntentIntegrator(activity)
        intentIntegrator.setPrompt(resources.getString(R.string.qr_warning))
        intentIntegrator.setBeepEnabled(true)
        intentIntegrator.setOrientationLocked(true)
        intentIntegrator.captureActivity = QRActivity::class.java
        intentIntegrator.initiateScan()
    }
}