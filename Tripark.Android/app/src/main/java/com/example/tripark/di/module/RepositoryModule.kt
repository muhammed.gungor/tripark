package com.example.tripark.di.module

import com.example.tripark.data.api.ApiService
import com.example.tripark.data.repository.ParkLocationRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class RepositoryModule {

    @Singleton
    @Provides
    fun providesRepository(apiService: ApiService) = ParkLocationRepository(apiService)
}