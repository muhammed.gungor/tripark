package com.example.tripark.ui.main.viewmodel

import androidx.lifecycle.*
import com.example.tripark.data.repository.ParkLocationRepository
import com.example.tripark.data.model.ParkLocation
import com.example.tripark.utils.NetworkHelper
import com.example.tripark.utils.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ParkLocationViewModel @Inject constructor(
    val parkLocationRepository: ParkLocationRepository,
    val networkHelper: NetworkHelper
) : ViewModel() {
    private val _locations = MutableLiveData<Resource<ParkLocation>>()
    val locations: LiveData<Resource<ParkLocation>>
        get() = _locations

    init {
        fetchParkLocations()
    }

    private fun fetchParkLocations() {
        viewModelScope.launch {
            _locations.postValue(Resource.loading(null))
            if (networkHelper.isNetworkConnected()) {
                parkLocationRepository.getLocations().let {
                    if (it.isSuccessful) {
                        _locations.postValue(Resource.success(it.body()))
                    } else _locations.postValue(Resource.error(it.errorBody().toString(), null))
                }
            } else _locations.postValue(Resource.error("No internet connection", null))
        }
    }
}