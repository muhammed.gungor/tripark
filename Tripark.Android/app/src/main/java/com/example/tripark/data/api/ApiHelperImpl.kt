package com.example.tripark.data.api

import com.example.tripark.data.model.ParkLocation
import retrofit2.Response
import javax.inject.Inject

class ApiHelperImpl @Inject constructor(private val apiService: ApiService) : ApiHelper {
    override suspend fun getLocations(): Response<ParkLocation> = apiService.getLocations()
}