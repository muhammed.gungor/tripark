package com.example.tripark.utils

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}