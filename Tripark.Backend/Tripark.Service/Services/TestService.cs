﻿using System;
using System.Threading.Tasks;
using Tripark.Service.Interfaces;

namespace Tripark.Service.Services
{
    public class TestService : ITestService
    {
        public async Task<string> ReturnStringVariable(string clientParameter = "Empty String")
        {
            return new String($"Server response is : {clientParameter}");
        }
    }
}
