﻿using AutoMapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Tripark.Core.UnitofWork;
using Tripark.Domain.Dto;
using Tripark.Domain.Entities;
using Tripark.Domain.Localize;
using Tripark.Domain.Mapping;
using Tripark.Service.Interfaces;

namespace Tripark.Service.Services
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _uow;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration; 
        private readonly IStringLocalizer<Resource> _localizer;

        public UserService(IUnitOfWork uow, IMapper mapper, IConfiguration configuration,IStringLocalizer<Resource> localizer)
        {
            _uow = uow;
            _mapper = mapper;
            _configuration = configuration;
            _localizer = localizer;
        }
         
        public async Task<UserDto> GetUser(Guid userId)
        {
            var user = await _uow.UserRepository.GetByIdAsync(userId);

            var userDto = _mapper.Map<UserDto>(user);

            return userDto;
        }

        public async Task<List<UserManagementListItem>> GetUserManagementList()
        {
            var userList = await _uow.UserRepository.GetMultiple(includeProperties:"UserRoles,UserRoles.Role");
            var mapResponse = TriparkMapping.Mapper.Map<IEnumerable<User>,List<UserManagementListItem>>(userList);
            return mapResponse;
        }
    }
}
