﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Tripark.Core.UnitofWork;
using Tripark.Domain.Common;
using Tripark.Domain.Dto;
using Tripark.Domain.Entities;
using Tripark.Domain.Localize;
using Tripark.Service.Interfaces;
using Tripark.Utility.Constans;
using Tripark.Utility.Enums;
using Tripark.Utility.Interfaces;
using Tripark.Utility.Security;
using static Tripark.Utility.Helpers.TriparkHttpHelper;

namespace Tripark.Service.Services
{
    public class AuthenticationService : IAuthenticationService
    {
        private readonly IUnitOfWork _uow;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        private readonly IStringLocalizer<Resource> _localizer;
        private readonly IConfigurationsHelper _configurationsHelper;
        private readonly ICommonTools _commonTools;

        public AuthenticationService(IUnitOfWork uow,
                                     IMapper mapper,
                                     IConfiguration configuration,
                                     IStringLocalizer<Resource> localizer,
                                     IConfigurationsHelper configurationsHelper,
                                     ICommonTools commonTools)
        {
            _uow = uow;
            _mapper = mapper;
            _configuration = configuration;
            _localizer = localizer;
            _configurationsHelper = configurationsHelper;
            _commonTools = commonTools;
        }

        public async Task<LoginResponse> LoginAsync(LoginRequest loginRequest)
        {
            var hashedPassword = EncryptionHelper.Encrypt(loginRequest.Password);

            var user = await _uow.UserRepository.Get(c => c.IsActive && c.Email.Equals(loginRequest.Email) && c.Password.Equals(hashedPassword));

            if (user is null)
                throw new CustomApiException(GetStringCode(HttpStatusCode.NotFound), _localizer[LocalizeKeys.USER_NOT_FOUND]);

            if (_configurationsHelper.LoginChannel.Equals(ApiChannels.Mobile))
            {
                var role = await CheckUserByRole(user, new UserRoles[] { UserRoles.MOBILE_USER });

                return LoginForMobile(user,role);
            }
            else if (_configurationsHelper.LoginChannel.Equals(ApiChannels.ClientWebApp))
            {
                var role = await CheckUserByRole(user, new UserRoles[] { UserRoles.SYSTEM_ADMIN, UserRoles.WEB_CLIENT_ADMIN });

                return await LoginForClientWeb(user,role);
            }
            else
            {
                throw new CustomApiException(GetStringCode(HttpStatusCode.BadRequest), _localizer[LocalizeKeys.GENERAL_LOGIN_FAILED]);
            }
        }

        public async Task<RegisterResponse> RegisterAsync(RegisterRequest request)
        {
            if (await CheckUserIsNotExist(request.Email, request.GsmNumber))
            {
                var hashedPassword = EncryptionHelper.Encrypt(request.Password);

                var user = new User
                {
                    Email = request.Email,
                    GsmNumber = request.GsmNumber,
                    Password = hashedPassword
                };

                await _uow.UserRepository.AddUserWithUserRole(user, _commonTools.GetUserRoleByChannel());

                var result = await _uow.SaveChangesAsync();

                if (result > 0)
                {
                    var response = _mapper.Map<RegisterResponse>(user);
                    response.Token = new JwtSecurityTokenHandler().WriteToken(GetToken(user.Email, user.Id.ToString()));

                    return response;
                }
                else
                {
                    throw new CustomApiException(StatusCodes.Status500InternalServerError.ToString(), _localizer[LocalizeKeys.GENERAL_ERROR_MESSAGE]);
                }
            }
            else
            {
                throw new CustomApiException(StatusCodes.Status400BadRequest.ToString(), _localizer[LocalizeKeys.AUTHENTICATION_REGISTER_USER_EXIST]);
            }
        }

        private async Task<Role> CheckUserByRole(User user, UserRoles[] userRoles)
        {
            var role = await _uow.GetRepository<UserRole>().Get(c => c.UserId.Equals(user.Id),includeProperties:"Role");

            var isExist = userRoles.Any(c => role.RoleId.Equals((int)c));

            if (!isExist)
                throw new CustomApiException(GetStringCode(HttpStatusCode.NotFound), _localizer[LocalizeKeys.USER_NOT_FOUND]);

            return role.Role;
        }

        private LoginResponse LoginForMobile(User user,Role role)
        {
            var response = new MobileLoginResponse()
            {
                Token = new JwtSecurityTokenHandler().WriteToken(GetToken(user.Email, user.Id.ToString())),
                UserName = user.Name,
                UserSurname = user.Lastname,
                BaseUrl = GetBaseUrlByRole((UserRoles)role.Id)
            };

            return response;
        }

        private async Task<LoginResponse> LoginForClientWeb(User user, Role role)
        {
            var response = new WebClientLoginResponse()
            {
                Token = new JwtSecurityTokenHandler().WriteToken(GetToken(user.Email, user.Id.ToString())),
                UserName = user.Name,
                UserSurname = user.Lastname,
                BaseUrl = GetBaseUrlByRole((UserRoles)role.Id)
            };

            return response;
        }

        private string GetBaseUrlByRole(UserRoles userRole)
        {
            switch (userRole)
            {
                case UserRoles.SYSTEM_ADMIN:
                    return "/admin";
                case UserRoles.WEB_CLIENT_ADMIN:
                case UserRoles.MOBILE_USER:
                    return "/main";
                default:
                    throw new CustomApiException(GetStringCode(HttpStatusCode.NotFound), _localizer[LocalizeKeys.GENERAL_INTERNAL_ERROR_MESSAGE]);
            }
        }

        private async Task<bool> CheckUserIsNotExist(string email, string gsmNumber)
        {
            var user = await _uow.UserRepository.Get(c => (c.Email.Equals(email.Trim()) || c.GsmNumber.Equals(gsmNumber.Trim())) && !c.IsDeleted);

            return (user is null);
        }

        private JwtSecurityToken GetToken(string userEmail, string userId)
        {
            var claims = new[]
            {
                new Claim("sub",userEmail),
                new Claim("jti",Guid.NewGuid().ToString()),
                new Claim("userId",userId)
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration[AppSettingsConstans.JwtBase.SECURITY_INFO_KEY]));
            var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var timeout = Convert.ToInt32(_configuration[AppSettingsConstans.JwtBase.TIMEOUT]);

            var generatedToken = new JwtSecurityToken(
                issuer: _configuration[AppSettingsConstans.JwtBase.SECURITY_INFO_ISSUER],
                audience: _configuration[AppSettingsConstans.JwtBase.SECURITY_INFO_AUDINCE],
                claims: claims,
                expires: DateTime.UtcNow.AddMinutes(timeout),
                signingCredentials: credentials
            );

            return generatedToken;
        }
    }
}
