﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Localization;
using Nest;
using NetTopologySuite;
using NetTopologySuite.Geometries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Tripark.Core.UnitofWork;
using Tripark.Domain.Common;
using Tripark.Domain.Dto;
using Tripark.Domain.Entities;
using Tripark.Domain.Localize;
using Tripark.Domain.Mapping;
using Tripark.Domain.Models.Bases;
using Tripark.Domain.Models.CompanyBranch.Dto;
using Tripark.Service.Interfaces;
using Tripark.Utility.Constans;
using static Tripark.Utility.Helpers.TriparkHttpHelper;

namespace Tripark.Service.Services
{
    public class CompanyService : ICompanyService
    {
        private readonly IUnitOfWork _uow;
        private readonly IStringLocalizer<Resource> _localizer;
        private readonly string userId;

        public CompanyService(IUnitOfWork unitOfWork,
                              IHttpContextAccessor httpContextAccessor)
        {
            _uow = unitOfWork;
            userId = httpContextAccessor.HttpContext.User.FindFirst("userId").Value;
        }

        public async Task<List<CompanyResponse>> GetMyCompaniesAndBranches()
        {
            if (string.IsNullOrEmpty(userId))
                throw new CustomApiException(GetStringCode(HttpStatusCode.Forbidden), _localizer[LocalizeKeys.TOKEN_EXPIRED_LOGIN_REQUIRED]);

            var userOwnedCompanies = await _uow.CompanyRepository.GetUserCompaniesWithBranches(Guid.Parse(userId));

            return userOwnedCompanies;
        }

        public async Task<CompanyBranchMainResponse> GetCarParksNearMe(CompanyBranchMainRequest model)
        {
            //var coord = new GeoCoordinate(model.Latitude, model.Longitute);
            //double coef = model.Distance * 0.0000089;
            //double new_lat = model.Latitude + coef;
            //double new_long = model.Longitute + coef / Math.Cos(model.Latitude * 0.018); 
            //var newCoord = new GeoCoordinate(new_lat, new_long);

            var currentLocation = CreatePoint(model.Latitude, model.Longitute);

            var result = await _uow.CompanyRepository.GetMultiple(x => CreatePoint(x.Latitude, x.Longitude).Distance(currentLocation) < model.Distance, null);
            var responseModel = new CompanyBranchMainResponse();
            responseModel.CompanyBranchDtos = result.Select(q => new CompanyBranchDto
            {
                Name = q.Name,
                UniqueAddressId = q.UniqueAddressId
            }).ToList();
            return responseModel;
        }
        public static NetTopologySuite.Geometries.Point CreatePoint(double latitude, double longitude)
        {
            // 4326 is most common coordinate system used by GPS/Maps
            var geometryFactory = NtsGeometryServices.Instance.CreateGeometryFactory(srid: 4326);

            // see https://docs.microsoft.com/en-us/ef/core/modeling/spatial
            // Longitude and Latitude
            var newLocation = geometryFactory.CreatePoint(new Coordinate(longitude, latitude));

            return newLocation;
        }

        public async Task<List<CompanyResponseAdmin>> GetCompanyList()
        {
            var list = await _uow.CompanyRepository.GetAllAsync();

            var response = list.Select(c => new CompanyResponseAdmin
            {
                IsActive = !c.IsDeleted,
                PhoneNumber = c.PhoneNumber,
                Title = c.Title
            }).ToList();

            return response;
        }

        public async Task<CompanyProfileResponse> GetCompanyProfile(string companyId)
        {
            Guid id;

            if (Guid.TryParse(companyId, out id))
            {
                var company = await _uow.CompanyRepository.GetByIdAsync(id);

                if (company is null)
                {
                    throw new CustomApiException(GetStringCode(HttpStatusCode.NotFound), _localizer[LocalizeKeys.COMPANY_NOT_FOUND]);
                }

                return TriparkMapping.Mapper.Map<CompanyProfileResponse>(company);
            }

            throw new CustomApiException(GetStringCode(HttpStatusCode.BadRequest), _localizer[LocalizeKeys.GENERAL_ARGUMENT_NULL]);
        }

        public async Task<bool> UpdateCompanyProfile(CompanyRequest company)
        {
            var currentCompany = await _uow.CompanyRepository.GetByIdAsync(company.Id);

            if (currentCompany is null)
                throw new CustomApiException(GetStringCode(HttpStatusCode.NotFound), _localizer[LocalizeKeys.COMPANY_NOT_FOUND]);

            currentCompany.Description = company.Description;
            currentCompany.Logo = company.Logo;
            currentCompany.Name = company.Name;
            currentCompany.PhoneNumber = company.PhoneNumber;

            _uow.CompanyRepository.Update(currentCompany);

            return await _uow.SaveChangesAsync() > 0;
        }
    }
}
