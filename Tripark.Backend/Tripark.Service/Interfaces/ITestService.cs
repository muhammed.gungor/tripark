﻿using System.Threading.Tasks;

namespace Tripark.Service.Interfaces
{
    public interface ITestService
    {
        Task<string> ReturnStringVariable(string clientParameter);
    }
}
