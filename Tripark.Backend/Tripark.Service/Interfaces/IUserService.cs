﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Tripark.Domain.Dto;

namespace Tripark.Service.Interfaces
{
    public interface IUserService
    {
        Task<UserDto> GetUser(Guid userId);

        /// <summary>
        /// This method provides to get all registered users from Tripark system
        /// </summary>
        /// <returns></returns>
        Task<List<UserManagementListItem>> GetUserManagementList();
    }
}
