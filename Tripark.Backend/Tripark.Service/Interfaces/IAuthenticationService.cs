﻿using System.Threading.Tasks;
using Tripark.Domain.Dto;

namespace Tripark.Service.Interfaces
{
    public interface IAuthenticationService
    {
        /// <summary>
        /// Register service
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        Task<RegisterResponse> RegisterAsync(RegisterRequest request);

        /// <summary>
        /// Login service for web admin client users
        /// </summary>
        /// <param name="loginRequest"></param>
        /// <returns></returns>
        Task<LoginResponse> LoginAsync(LoginRequest loginRequest);
    }
}
