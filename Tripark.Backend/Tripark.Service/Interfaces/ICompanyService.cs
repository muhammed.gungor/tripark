﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tripark.Domain.Dto;
using Tripark.Domain.Entities;
using Tripark.Domain.Models.CompanyBranch.Dto;

namespace Tripark.Service.Interfaces
{
    public interface ICompanyService
    {
        /// <summary>
        /// Get web client user's companies
        /// </summary>
        /// <returns></returns>
        Task<List<CompanyResponse>> GetMyCompaniesAndBranches();

        /// <summary>
        /// Get park areas with user location informations
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<CompanyBranchMainResponse> GetCarParksNearMe(CompanyBranchMainRequest model);

        /// <summary>
        /// Get companies for admin company table
        /// </summary>
        /// <returns></returns>
        Task<List<CompanyResponseAdmin>> GetCompanyList();

        /// <summary>
        /// Get company profile
        /// </summary>
        /// <returns></returns>
        Task<CompanyProfileResponse> GetCompanyProfile(string companyId);

        /// <summary>
        /// Update company profile
        /// </summary>
        /// <returns></returns>
        Task<bool> UpdateCompanyProfile(CompanyRequest company);
    }
}
