﻿using Microsoft.AspNetCore.Http;
using Tripark.Domain.Interface;
using Tripark.Utility.Enums;

namespace Tripark.Utility.Interfaces
{
    public interface ICommonTools
    {
       
       
        UserRoles GetUserRoleByChannel();

        bool SendSms(IParameterResponse parameter, string number, string message);
        bool SendMail(MailTemplates mailTemplate, string email, string subject, string body = "", string title = "", string code = "", bool throwException = true);
        string CreateRandomPassword();
    }
}
