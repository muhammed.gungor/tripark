﻿using Tripark.Utility.Enums;
using Microsoft.AspNetCore.Hosting;

namespace Tripark.Utility.Interfaces
{
    public interface IConfigurationsHelper
    {
        string GetHostName();
        void SetChannel();

        /// <summary>
        /// Set hosting environment to global variable for using run time
        /// </summary>
        /// <param name="env"></param>
        void SetEnvironment(IWebHostEnvironment env);

        /// <summary>
        /// Get hosting environment
        /// </summary>
        EnvironmentTypes GetEnvironment();
        public ApiChannels LoginChannel { get; set; }
    }
}
