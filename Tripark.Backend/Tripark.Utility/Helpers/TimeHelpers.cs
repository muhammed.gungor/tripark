﻿using System;

namespace Tripark.Utility.Helpers
{
    public class TimeHelpers
    {
        public static DateTime GetTurkeyTime()
        {
            var date = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, TimeZoneInfo.FindSystemTimeZoneById("Turkey Standard Time"));

            return date;
        }
    }
}
