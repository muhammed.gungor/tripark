﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Tripark.Utility.Helpers
{
    public static class JsonHelper
    {
        public static bool ValidateJson(this string s)
        {
            bool response;
            try
            {
                JToken.Parse(s);
                response = true;
            }
            catch (JsonReaderException ex)
            {
                response = false;
            }

            return response;
        }
        //obj to string
        public static string ToJson(this object value)
        {
            return JsonConvert.SerializeObject(value);
        }
        //string to obj
        public static T ToObject<T>(this string value) where T : class
        {
            return string.IsNullOrEmpty(value) ? null : JsonConvert.DeserializeObject<T>(value);
        }

    }
}
