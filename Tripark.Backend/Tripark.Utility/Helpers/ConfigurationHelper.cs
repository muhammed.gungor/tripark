﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using System;
using Tripark.Utility.Constans;
using Tripark.Utility.Enums;
using Tripark.Utility.Interfaces;

namespace Tripark.Utility.Helpers
{
    public class ConfigurationsHelper : IConfigurationsHelper
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IConfiguration _configuration;

        public ApiChannels LoginChannel { get; set; }
        private EnvironmentTypes EnvironmentType { get; set; }

        public ConfigurationsHelper(IHttpContextAccessor httpContextAccessor, IConfiguration configuration)
        {
            _httpContextAccessor = httpContextAccessor;
            _configuration = configuration;
        }

        public string GetHostName()
        {
            return _httpContextAccessor.HttpContext.Request.Scheme + "://" + _httpContextAccessor.HttpContext.Request.Host.Value;
        }


        public void SetEnvironment(IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                EnvironmentType = EnvironmentTypes.DEVELOPMENT;
            }
            else if (env.IsStaging())
            {
                EnvironmentType = EnvironmentTypes.STAGING;
            }
            else if (env.IsProduction())
            {
                EnvironmentType = EnvironmentTypes.PRODUCTION;
            }
            else
            {
                throw new Exception("ENVIRONMENT TURU BELIRLENEMEDI. PROJE KAPATILIYOR.");
            }
        }

        public void SetChannel()
        {
            var subDomain = string.Empty;

            var host = _httpContextAccessor.HttpContext.Request.Host.Host;

            if (!string.IsNullOrWhiteSpace(host))
            {
                subDomain = host.Split('.')[0];
            }

            var channelString = subDomain.Trim().ToLower();

            if (channelString == _configuration[AppSettingsConstans.ChannelConstants.MOBILE].ToString())
            {
                this.LoginChannel = ApiChannels.Mobile;
            }
            else if (channelString == _configuration[AppSettingsConstans.ChannelConstants.CLIENT_WEB_APP].ToString())
            {
                this.LoginChannel = ApiChannels.ClientWebApp;
            }
            else
            {
                this.LoginChannel = ApiChannels.Unknown;
            }
        }

        public EnvironmentTypes GetEnvironment()
        {
            return this.EnvironmentType;
        }
    }
}
