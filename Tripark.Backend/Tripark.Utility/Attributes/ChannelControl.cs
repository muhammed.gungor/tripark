﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Localization;
using Tripark.Domain.Localize;
using Tripark.Utility.Constans;
using Tripark.Utility.Enums;
using Tripark.Utility.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Mvc;
using System;

namespace Tripark.Utility.Attributes
{
    public class ChannelControl : IActionFilter
    {
        public void OnActionExecuted(ActionExecutedContext context)
        {
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            var configurationHelperService = context.HttpContext.RequestServices.GetService<IConfigurationsHelper>();
            var localizerService = context.HttpContext.RequestServices.GetService<IStringLocalizer<Resource>>();

            if (!configurationHelperService.LoginChannel.Equals(ApiChannels.ClientWebApp))
            {
                context.HttpContext.Items.Add("exception", new Exception());
                context.HttpContext.Items.Add("exceptionMessage", localizerService[LocalizeKeys.CHANNEL_FORBIDDEN_MESSAGE]);
                context.HttpContext.Items.Add("channelExceptionStatusCode", StatusCodes.Status403Forbidden);

                context.Result = new OkResult();

                return;
            }
        }
    }
}
