﻿using System;

namespace Tripark.Utility.Constans
{
    public static class SeedData
    {
        public static readonly Guid TestUserId = Guid.Parse("01660ef8-cb4a-42c2-a82e-2a0e099ad4ab");
        public static readonly Guid AdminUserId = Guid.Parse("99d4d52f-d403-4084-90b3-21972f9d65f8");
        public static readonly Guid CompanyGroupId = Guid.Parse("01d4d52f-d403-4084-90b3-21972f9d6501");
        public static readonly Guid CompanyId = Guid.Parse("016FE50E-F13E-429E-A502-7DA96D6BD601");
        public static readonly Guid CompanyUserId = Guid.Parse("0136AF1B-42B4-4D7F-B99A-F168C9287165");
    }
}