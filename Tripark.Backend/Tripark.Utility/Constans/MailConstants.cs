﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tripark.Utility.Constans
{
    public static class MailConstants
    {
        public static class CommonValues
        {
            //
            public const int OTP_DIGIT_COUNT = 6;
            public const string OTP_RANDOM_COMPLICATED = "ABCDEFGHJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*?_-";
            public const string OTP_RANDOM_BASIC = "0123456789";
        }
    }
}
