﻿namespace Tripark.Utility.Constans
{
    public static class LocalizeKeys
    {
        public const string APPLICATON_NAME = "ApplicationName";
        public const string AUTHENTICATION_REGISTER_USER_EXIST = "AuthenticationRegister_UserExist";
        public const string GENERAL_ERROR_MESSAGE = "GeneralErrorMessage";
        public const string GENERAL_INTERNAL_ERROR_MESSAGE = "GeneralInternalErrorMessage";
        public const string GENERAL_SUCCESS_MESSAGE = "GeneralSuccessMessage";
        public const string CHANNEL_FORBIDDEN_MESSAGE = "ChannelForbiddenMessage";
        public const string USER_NOT_FOUND = "UserNotFound";
        public const string GENERAL_LOGIN_FAILED = "GeneralLoginFailed";
        public const string TOKEN_EXPIRED_LOGIN_REQUIRED = "TokenExpiredLoginRequired";
        public const string COMPANY_NOT_FOUND = "CompanyNotFound";
        public const string GENERAL_ARGUMENT_NULL = "GeneralArgumentNull";
    }
}