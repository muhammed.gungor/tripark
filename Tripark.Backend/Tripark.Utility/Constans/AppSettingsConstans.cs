﻿namespace Tripark.Utility.Constans
{
    public static class AppSettingsConstans
    {
        public static class JwtBase
        {
            public const string SECURITY_INFO_ISSUER = "SecurityInfo:Issuer";
            public const string SECURITY_INFO_AUDINCE = "SecurityInfo:Audince";
            public const string SECURITY_INFO_KEY = "SecurityInfo:Key";
            public const string TIMEOUT = "Timeout";
        }

        public static class ChannelConstants
        {
            public const string MOBILE = "mobile";
            public const string CLIENT_WEB_APP = "client_webapp";
        }

        public static class RedisConstants
        {
            public const string REDIS_CONNECTION_STRING = "Redis:Host";
           
        }
        public static class MailConfigurationKeys
        {
            public const string MAIL_URL = "mailUrl";
            public const string FROM_MAIL_ADDRESS = "FromMailAddress";
            public const string FROM_MAIL_DISPLAY_NAME = "FromMailDisplayName";
            public const string FROM_MAIL_PASSWORD = "FromMailPassword";
        }

       
    }

   
}
