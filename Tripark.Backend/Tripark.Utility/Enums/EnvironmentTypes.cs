﻿namespace Tripark.Utility.Enums
{
    public enum EnvironmentTypes
    {
        DEVELOPMENT,
        STAGING,
        PRODUCTION
    }

}
