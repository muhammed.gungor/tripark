﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tripark.Utility.Enums
{
    public enum UserRoles
    {
        SYSTEM_ADMIN = 99,
        WEB_CLIENT_ADMIN =2,
        MOBILE_USER = 1
    }
}
