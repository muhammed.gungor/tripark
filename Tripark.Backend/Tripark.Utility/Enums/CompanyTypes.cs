﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tripark.Utility.Enums
{
    public enum CompanyTypes
    {
        AUTOPARK = 1,
        GAS_STATION = 2,
        CLEANING_SERVICES = 3,
        REST_AND_SERVICE_AREA = 4
    }
}
