﻿namespace Tripark.Utility.Enums
{
    public enum ApiChannels
    {
        Mobile,
        ClientWebApp,
        Unknown
    }
}
