﻿namespace Tripark.Utility.Enums
{
    public enum MailTemplates
    {
        WelcomeMail = 1,
        ForgetPassword = 2,
        UserActive = 3 
    }
}
