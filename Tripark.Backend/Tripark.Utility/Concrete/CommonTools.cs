﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Text;
using Tripark.Domain.Interface;
using Tripark.Utility.Constans;
using Tripark.Utility.Enums;
using Tripark.Utility.Interfaces;

namespace Tripark.Utility.Concrete
{
    public class CommonTools : ICommonTools
    {
        private readonly IConfiguration _configuration;
        private readonly IHttpContextAccessor _httpContext;
        private readonly IConfigurationsHelper _configurationHelper;


        public CommonTools(IConfiguration configuration, IHttpContextAccessor httpContext,IConfigurationsHelper  configurationsHelper)
        {
            _configuration = configuration;
            _httpContext = httpContext;
            _configurationHelper = configurationsHelper;
        }

        public UserRoles GetUserRoleByChannel()
        {
            var channel = _configurationHelper.LoginChannel;

            if (channel.Equals(ApiChannels.Mobile))
            {
                return UserRoles.MOBILE_USER;
            }
            else if (channel.Equals(ApiChannels.ClientWebApp))
            {
                return UserRoles.WEB_CLIENT_ADMIN;
            }

            //It will be change according to next roles
            return UserRoles.MOBILE_USER;
        }

        public string CreateRandomPassword()
        {
            var validChars = "0123456789";
            var random = new Random();
            var chars = new char[6];
            for (int i = 0; i < 5; i++)
            {
                chars[i] = validChars[random.Next(0, validChars.Length)];
            }
            return new string(chars);
        }

        public bool SendMail(MailTemplates mailTemplate, string email, string subject, string body = "", string title = "", string code = "", bool throwException = true)
        {
            string url = _configuration[AppSettingsConstans.MailConfigurationKeys.MAIL_URL];

            MailAddress fromAddress = new MailAddress(_configuration[AppSettingsConstans.MailConfigurationKeys.FROM_MAIL_ADDRESS], _configuration[AppSettingsConstans.MailConfigurationKeys.FROM_MAIL_DISPLAY_NAME]);
            MailAddress toAddress = new MailAddress(email);

            SmtpClient smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(_configuration[AppSettingsConstans.MailConfigurationKeys.FROM_MAIL_ADDRESS], _configuration[AppSettingsConstans.MailConfigurationKeys.FROM_MAIL_PASSWORD])
            };

            string bodyWithTemplate = body;

            switch (mailTemplate)
            {
                case MailTemplates.WelcomeMail:
                    using (StreamReader reader = new StreamReader(Path.Combine(url, "WelcomeMail.html")))
                    {
                        bodyWithTemplate = reader.ReadToEnd();
                    }

                    break;
                case MailTemplates.ForgetPassword:
                    using (StreamReader reader = new StreamReader(Path.Combine(url, "ForgetPassword.html")))
                    {
                        bodyWithTemplate = reader.ReadToEnd();
                    }

                    bodyWithTemplate = bodyWithTemplate.Replace("{CODE}", code);

                    break;
                default:
                    bodyWithTemplate = body;
                    break;
            }

            bodyWithTemplate = !string.IsNullOrEmpty(title) ? bodyWithTemplate.Replace("{TITLE}", title) : bodyWithTemplate.Replace("{TITLE}", subject);
            bodyWithTemplate = bodyWithTemplate.Replace("{BODY}", body);

            using var message = new MailMessage(fromAddress, toAddress)
            {
                IsBodyHtml = true,
                Subject = subject,
                Body = bodyWithTemplate
            };

            try
            {
                smtp.Send(message);
                return true;
            }
            catch (Exception ex)
            {

                if (throwException)
                {
                    throw ex;
                }
                else
                {
                    //_logger.LogError(Constants.GeneralStrings.EMAIL_SENDING_FAILURE_MESSAGE, ex);
                    return false;
                }
            }
        }

        public bool SendSms(IParameterResponse parameter, string number, string message)
        {
            throw new NotImplementedException();
        }
    }
}
