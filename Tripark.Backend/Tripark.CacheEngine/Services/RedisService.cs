﻿using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;
using System.Text;
using System.Threading.Tasks;
using Tripark.CacheEngine.Interfaces;

namespace Tripark.CacheEngine.Services
{
    public class RedisService : IRedisCacheServices
    {
        private readonly IDistributedCache _cache;

        public RedisService(IDistributedCache cache)
        {
            _cache = cache;
        }

        public async Task<string> GetKeyString(string key)
        {
            var byteData = await _cache.GetAsync(key)
;

            if (byteData == null)
            {
                return string.Empty;
            }

            var serializableData = System.Text.Encoding.UTF8.GetString(byteData);

            return serializableData;
        }

        public async Task SetKey<T>(string key, T obj)
        {
            var serializedData = JsonConvert.SerializeObject(obj);

            var byteData = Encoding.UTF8.GetBytes(serializedData);

            await _cache.SetAsync(key, byteData);
        }

        public async Task SetString(string key, string value)
        {
            await _cache.SetStringAsync(key, value);
        }
    }
}
