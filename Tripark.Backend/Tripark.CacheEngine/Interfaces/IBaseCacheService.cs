﻿using System.Threading.Tasks;

namespace Tripark.CacheEngine.Interfaces
{
    public interface IBaseCacheService
    {
        Task SetKey<T>(string key, T obj);
        Task<string> GetKeyString(string key); 
        Task SetString(string key, string value);
    }
}
