﻿using FluentValidation;

namespace Tripark.Domain.Common
{
    public class BaseValidation<T> : AbstractValidator<T> where T : class
    {
        public virtual string GsmNumberRegexErrorMessage { get; set; } = "Lütfen geçerli telefon numarası giriniz.";
        public virtual string EmailErrorMessage { get; set; } = "Lütfen geçerli e-posta adresi giriniz.";


        public virtual string SetNotEmptyMessage(string propertyName)
        {
            return $"{propertyName} alanı boş olamaz.";
        }
    }
}
