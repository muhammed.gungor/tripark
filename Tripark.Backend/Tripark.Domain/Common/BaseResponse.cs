﻿using Newtonsoft.Json;
using System;

namespace Tripark.Domain.Common
{
    public class BaseResponse
    {
        [JsonProperty]
        public Guid Id { get; set; }
    }
}
