﻿using Newtonsoft.Json;

namespace Tripark.Domain.Common
{
    public class TokenBase
    {
        [JsonProperty("token")]
        public string Token { get; set; }
    }
}
