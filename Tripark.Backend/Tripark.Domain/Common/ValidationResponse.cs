﻿using System.Net;

namespace Tripark.Domain.Common
{
    public class ValidationResponse
    {
        public HttpStatusCode StatusCode { get; set; }
        public string Message { get; set; }
    }
}
