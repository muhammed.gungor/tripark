﻿using Newtonsoft.Json;
using System;
using System.Net;

namespace Tripark.Domain.Common
{
    public class CommonResponse
    {
        public static CommonResponse Create(HttpStatusCode statusCode, object result = null, string errorMessage = null, string correletionId = null, string errorCode = null)
        {
            return new CommonResponse(statusCode, result, errorMessage, correletionId, errorCode);
        }

        [JsonProperty("version")]
        public string Version => "1.0";

        [JsonProperty("errorMessage")]
        public string ErrorMessage { get; set; }

        [JsonProperty("correlationId")]
        public string CorreletionId { get; set; }

        [JsonProperty("result")]
        public object Result { get; set; }

        [JsonProperty("statusCode")]
        public HttpStatusCode StatusCode { get; set; }
        protected CommonResponse(HttpStatusCode statusCode, object result = null, string errorMessage = null, string correletionId = null, string errorCode = null)
        {
            CorreletionId = Guid.NewGuid().ToString();
            Result = result;
            ErrorMessage = errorMessage;
            StatusCode = statusCode;
        }
    }
}
