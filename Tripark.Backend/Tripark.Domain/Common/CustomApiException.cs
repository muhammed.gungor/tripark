﻿using System;

namespace Tripark.Domain.Common
{
    public class CustomApiException : Exception
    {
        public string Code { get; }

        protected CustomApiException()
        {
        }

        public CustomApiException(string code)
        {
            Code = code;
        }

        public CustomApiException(string message, params object[] args)
            : this(string.Empty, message, args)
        {
        }

        public CustomApiException(string code, string message, params object[] args)
            : this(null, code, message, args)
        {
        }

        public CustomApiException(Exception innerException, string message, params object[] args)
            : this(innerException, string.Empty, message, args)
        {
        }

        public CustomApiException(Exception innerException, string code, string message, params object[] args)
            : base(string.Format(message, args), innerException)
        {
            Code = code;
        }
    }
}
