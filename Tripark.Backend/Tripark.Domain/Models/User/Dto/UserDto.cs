﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tripark.Domain.Models.User.Dto
{
    public class UserDto
    {
        public Guid Id { get; set; }
        public string Email { get; set; }
        public string GsmNumber { get; set; }
        public string Name { get; set; }
        public string Lastname { get; set; }

    }
}
