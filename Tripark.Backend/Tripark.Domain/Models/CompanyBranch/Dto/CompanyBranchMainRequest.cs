﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tripark.Domain.Models.CompanyBranch.Dto
{
    public class CompanyBranchMainRequest
    { 
        public double Latitude { get; set; }
        public double Longitute { get; set; }
        public double Distance { get; set; }
    }
}
