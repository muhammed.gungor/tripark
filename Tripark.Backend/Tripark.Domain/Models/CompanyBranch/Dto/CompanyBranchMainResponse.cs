﻿using System;
using System.Collections.Generic;
using System.Text;
using Tripark.Domain.Models.Bases;

namespace Tripark.Domain.Models.CompanyBranch.Dto
{
    public class CompanyBranchMainResponse
    {
        public List<CompanyBranchDto> CompanyBranchDtos { get; set; }
    }
}
