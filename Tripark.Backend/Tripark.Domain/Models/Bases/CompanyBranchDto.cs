﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tripark.Domain.Models.Bases
{
    public class CompanyBranchDto
    {
        public Guid CompanyId { get; set; }
        public string Title { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string Description { get; set; }
        public int TypeId { get; set; }
        public string Logo { get; set; }
        public Guid AdressId { get; set; }
        public string FullAddress { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public int CityId { get; set; }
        public int CountyId { get; set; }
        public string UniqueAddressId { get; set; }
    }
}
