﻿using System.ComponentModel.DataAnnotations;
using Tripark.Domain.Common;
using FluentValidation;

namespace Tripark.Domain.Dto
{
    public class RegisterRequest
    {
        /// <summary>
        /// User email
        /// </summary>
        [Required]
        public string Email { get; set; }

        /// <summary>
        /// User password
        /// </summary>
        [Required]
        public string Password { get; set; }

        [Required]
        public string GsmNumber { get; set; }
    }

    public class RegisterRequestValidator : BaseValidation<RegisterRequest>
    {
        public RegisterRequestValidator()
        {
            RuleFor(c => c.Email).NotEmpty().EmailAddress(FluentValidation.Validators.EmailValidationMode.AspNetCoreCompatible).WithMessage(base.EmailErrorMessage);
            RuleFor(c => c.Password).NotEmpty().WithMessage((obj) => 
                {                   
                   return base.SetNotEmptyMessage("Şifre");
                });
        }
    }
}
