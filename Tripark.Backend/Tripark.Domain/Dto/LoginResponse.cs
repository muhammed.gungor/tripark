﻿using Newtonsoft.Json;

namespace Tripark.Domain.Dto
{
    public class LoginResponse
    {
        [JsonProperty("token")]
        public string Token { get; set; }

        [JsonProperty("username")]
        public string UserName { get; set; }

        [JsonProperty("usersurname")]
        public string UserSurname { get; set; }

        [JsonProperty("baseUrl")]
        public string BaseUrl { get; set; }
    }
}
