﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tripark.Domain.Dto
{
    public class CompanyResponse
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public bool IsMasterBranch { get; set; }
        public string PrettyAddress { get; set; }
    }
}
