﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tripark.Domain.Dto
{
    public class LoginCompanyResponse
    {
        public string Title { get; set; }
        public bool IsMasterBranch { get; set; }
        public bool IsFirstCreation { get; set; }
    }
}
