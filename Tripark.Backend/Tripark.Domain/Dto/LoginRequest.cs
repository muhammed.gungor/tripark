﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace Tripark.Domain.Dto
{
    public class LoginRequest
    {
        [JsonProperty("email")]
        public string Email { get; set; }

        [Required]
        [JsonProperty("password")]
        public string Password { get; set; }
    }
}
