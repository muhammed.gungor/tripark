﻿namespace Tripark.Domain.Dto
{
    public class CompanyAddressRequest
    {
        public string FullAddress { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public int CityId { get; set; }
        public int CountyId { get; set; }
        public string UniqueAddressId { get; set; }
    }
}
