﻿using System;

namespace Tripark.Domain.Dto
{
    public class CompanyRequest
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string Description { get; set; }
        public int CompanyTypeId { get; set; }
        public string Logo { get; set; }
    }
}
