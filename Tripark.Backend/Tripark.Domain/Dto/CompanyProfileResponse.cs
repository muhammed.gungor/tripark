﻿using Newtonsoft.Json;

namespace Tripark.Domain.Dto
{
    public class CompanyProfileResponse : CompanyResponse
    {
        [JsonProperty("phoneNumber")]
        public string PhoneNumber { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("logo")]
        public string Logo { get; set; }
        [JsonProperty("companyName")]
        public string Name { get; set; }
    }
}
