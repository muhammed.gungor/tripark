﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Tripark.Domain.Dto
{
    public class WebClientLoginResponse : LoginResponse
    {

        [JsonProperty("company")]
        public LoginCompanyResponse Company { get; set; }
    }
}
