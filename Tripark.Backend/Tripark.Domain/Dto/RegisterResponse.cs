﻿using Tripark.Domain.Common;

namespace Tripark.Domain.Dto
{
    public class RegisterResponse : TokenBase
    {
        public string Name { get; set; }
        public string Surname{ get; set; }
    }
}
