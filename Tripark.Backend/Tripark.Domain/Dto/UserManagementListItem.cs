﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tripark.Domain.Dto
{
    public class UserManagementListItem
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Lastname { get; set; }
        public string GsmNumber { get; set; }
        public bool IsActive { get; set; }
        public DateTime AddDate { get; set; }
        public List<string> UserRoles{ get; set; }
    }
}
