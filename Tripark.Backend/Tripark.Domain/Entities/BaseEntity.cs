﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Tripark.Domain.Entities
{
    public class BaseEntity
    {
        /// <summary>
        /// Base Id of each record
        /// </summary>
        [Key]
        public Guid Id { get; set; }

        /// <summary>
        /// Addition date of each record
        /// </summary>
        public DateTime AddDate { get; set; }

        /// <summary>
        /// Update date of each record
        /// </summary>
        public DateTime UpdateDate { get; set; }
    }
}
