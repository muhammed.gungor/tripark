﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Tripark.Domain.Entities
{
    public class CompanyUser : BaseEntityWithDelete
    {
        public Guid CompanyId { get; set; }

        public Guid UserId { get; set; }

        [ForeignKey(nameof(CompanyId))]
        [InverseProperty("CompanyUsers")]
        public virtual Company Company { get; set; }

        [ForeignKey(nameof(UserId))]
        [InverseProperty("CompanyUsers")]
        public virtual User User { get; set; }
    }
}
