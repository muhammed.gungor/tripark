﻿namespace Tripark.Domain.Entities
{
    public class BaseEntityWithDelete : BaseEntity
    {
        /// <summary>
        /// Deletion status of record
        /// </summary>
        public bool IsDeleted { get; set; }
    }
}
