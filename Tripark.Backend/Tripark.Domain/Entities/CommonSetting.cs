﻿using System.ComponentModel.DataAnnotations;

namespace Tripark.Domain.Entities
{
    public class CommonSetting
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public string Group  { get; set; }
    }
}
