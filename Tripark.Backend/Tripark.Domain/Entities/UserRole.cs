﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Tripark.Domain.Entities
{
    public class UserRole : BaseEntity
    {
        [ForeignKey("User")]
        public Guid UserId { get; set; }

        [ForeignKey("Role")]
        public int RoleId { get; set; }

        public virtual User User { get; set; }
        public virtual Role Role { get; set; }
    }
}
