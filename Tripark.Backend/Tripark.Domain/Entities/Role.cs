﻿using System.Collections.Generic;

namespace Tripark.Domain.Entities
{
    public class Role
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<UserRole> UserRoles { get; set; }

        public Role()
        {
            this.UserRoles = new HashSet<UserRole>();
        }
    }
}