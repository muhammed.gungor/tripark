﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Tripark.Domain.Entities
{
    public class CompanyGroup : BaseEntityWithDelete
    {
        public CompanyGroup()
        {
            this.Companies = new HashSet<Company>();
        }

        public string GroupTitle { get; set; }

        [InverseProperty("CompanyGroup")]
        public virtual ICollection<Company> Companies { get; set; }
    }
}
