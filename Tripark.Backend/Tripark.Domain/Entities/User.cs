﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Tripark.Domain.Entities
{
    public class User : BaseEntityWithDelete
    {
        [EmailAddress]
        [Required]
        public string Email { get; set; }
        
        [Required]
        public string Password { get; set; }

        [Required]
        [MaxLength(50)]
        public string GsmNumber { get; set; }

        public string Name { get; set; }

        public string Lastname { get; set; }

        public bool IsActive { get; set; }

        public virtual ICollection<UserRole> UserRoles { get; set; }

        [InverseProperty("User")]
        public virtual ICollection<CompanyUser> CompanyUsers { get; set; }

        public User()
        {
            this.UserRoles = new HashSet<UserRole>();
            this.CompanyUsers = new HashSet<CompanyUser>();
        }
    }
}
