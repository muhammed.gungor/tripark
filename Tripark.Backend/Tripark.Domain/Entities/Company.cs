﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Tripark.Domain.Entities
{
    public class Company: BaseEntityWithDelete
    {
        public Company()
        {
            this.CompanyUsers = new HashSet<CompanyUser>();
        }

        public string Title { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string Description { get; set; }
        public int TypeId { get; set; }
        public string Logo { get; set; }
        public Guid CompanyGroupId { get; set; }
        public bool IsMasterBranch { get; set; }
        public string FullAddress { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public int CityId { get; set; }
        public int CountyId { get; set; }
        public string UniqueAddressId { get; set; }
        public string WorkingHours { get; set; }

        [InverseProperty("Company")]
        public virtual ICollection<CompanyUser> CompanyUsers { get; set; }

        [ForeignKey(nameof(CompanyGroupId))]
        [InverseProperty("Companies")]
        public virtual CompanyGroup CompanyGroup { get; set; }
    }
}
