﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using Tripark.Domain.Dto;
using Tripark.Domain.Entities;

namespace Tripark.Domain.Mapping
{
    public static class TriparkMapping
    {
        private static readonly Lazy<IMapper> Lazy = new Lazy<IMapper>(() =>
        {
            var config = new MapperConfiguration(cfg => {
                // This line ensures that internal properties are also mapped over.
                cfg.ShouldMapProperty = p => p.GetMethod.IsPublic || p.GetMethod.IsAssembly;
                cfg.AddProfile<MappingProfile>();
            });
            var mapper = config.CreateMapper();
            return mapper;
        });

        public static IMapper Mapper => Lazy.Value;
    }

    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<CompanyProfileResponse, Company>().ReverseMap();
            CreateMap<UserManagementListItem, User>().ReverseMap().ForMember(c=>c.UserRoles,x=>x.MapFrom(src=>src.UserRoles.Select(s=>s.Role.Name)));
        }
    }
}
