﻿namespace Tripark.Domain.Interface
{
    public interface IParameterResponse
    {
        /// <summary>
        /// Parameter Id
        /// </summary>
        int Id { get; set; }
        /// <summary>
        /// Parameter name
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// Parameter value
        /// </summary>
        string Value { get; set; }

        /// <summary>
        /// Parameter group
        /// </summary>
        string Group { get; set; }
    }
}
