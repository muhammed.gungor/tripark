﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Tripark.CacheEngine.Interfaces;
using Tripark.Domain.Entities;
using Tripark.Service.Interfaces;

namespace Tripark.Client.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TestRedisController : ControllerBase
    {
        private readonly IRedisCacheServices _redisCacheServices;
       

        public TestRedisController(IRedisCacheServices redisCacheServices)
        {
            _redisCacheServices = redisCacheServices;
        }
       
        [HttpGet]
        public async Task <IActionResult> SetRedisTest() 
        {
            //await _redisCacheServices.SetString("Mehmet", "Yunus"); 
            return Ok(await _redisCacheServices.GetKeyString("Mehmet"));
        }
    }
}
