﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Tripark.Domain.Dto;
using Tripark.Service.Interfaces;
using Tripark.Utility.Attributes;

namespace Tripark.Client.API.Controllers
{
    [Route("api/management/user")]
    [ApiController]
    public class UserManagementController : ControllerBase
    {
        private readonly IUserService _userService;

        public UserManagementController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet("list")]
        [ProducesResponseType(typeof(List<UserManagementListItem>), 200)]
        [Produces("application/json")]
        [ServiceFilter(typeof(ChannelControl))]
        [Authorize]
        public async Task<IActionResult> GetCompaniesForAdmin()
        {
            return Ok(await _userService.GetUserManagementList());
        }
    }
}
