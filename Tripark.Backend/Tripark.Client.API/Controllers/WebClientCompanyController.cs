﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Tripark.Domain.Dto;
using Tripark.Service.Interfaces;
using Tripark.Utility.Attributes;

namespace Tripark.Client.API.Controllers
{
    [Route("api/company")]
    [ApiController]
    public class WebClientCompanyController : ControllerBase
    {
        private readonly ICompanyService _companyService;

        public WebClientCompanyController(ICompanyService companyService)
        {
            _companyService = companyService;
        }

        [HttpGet]
        [Route("branch/list")]
        [ProducesResponseType(typeof(List<int>), 200)]
        [Produces("application/json")]
        [ServiceFilter(typeof(ChannelControl))]
        [Authorize]
        public async Task<IActionResult> GetMyCompaniesAsync()
        {
            return Ok(await _companyService.GetMyCompaniesAndBranches());
        }

        [HttpGet]
        [Route("list")]
        [ProducesResponseType(typeof(List<int>), 200)]
        [Produces("application/json")]
        [ServiceFilter(typeof(ChannelControl))]
        [Authorize]
        public async Task<IActionResult> GetCompaniesForAdmin()
        {
            return Ok(await _companyService.GetCompanyList());
        }

        [HttpGet]
        [Route("profile/{companyId}")]
        [ProducesResponseType(typeof(List<int>), 200)]
        [Produces("application/json")]
        [ServiceFilter(typeof(ChannelControl))]
        [Authorize]
        public async Task<IActionResult> GetCompanyProfile([Required] string companyId)
        {
            return Ok(await _companyService.GetCompanyProfile(companyId));
        }

        [HttpPut]
        [Route("profile")]
        [ProducesResponseType(typeof(List<int>), 200)]
        [Produces("application/json")]
        [ServiceFilter(typeof(ChannelControl))]
        [Authorize]
        public async Task<IActionResult> UpdateCompanyProfile([Required] CompanyRequest company)
        {
            return Ok(await _companyService.UpdateCompanyProfile(company));
        }
    }
}
