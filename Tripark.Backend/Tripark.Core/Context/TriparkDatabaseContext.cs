﻿using Microsoft.EntityFrameworkCore;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;
using Tripark.Core.EntityConfigurations;
using Tripark.Domain.Entities;

namespace Tripark.Core.Context
{
    public class TriparkDatabaseContext : DbContext
    {
        public TriparkDatabaseContext(DbContextOptions<TriparkDatabaseContext> options) : base(options)
        { 
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }
        public DbSet<CommonSetting> CommonSettings { get; set; }
        public DbSet<Company> Companies { get; set; }
        public DbSet<CompanyUser> CompanyUsers { get; set; }
        public DbSet<CompanyGroup> CompanyGroup { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //modelBuilder.ApplyConfigurationsFromAssembly(typeof(TriparkDatabaseContext).Assembly);
            modelBuilder.HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn);

            modelBuilder.ApplyConfiguration(new UserConfiguration());
            modelBuilder.ApplyConfiguration(new RoleConfiguration());
            modelBuilder.ApplyConfiguration(new UserRoleConfiguration());
            modelBuilder.ApplyConfiguration(new CommonSettingConfiguration());
            modelBuilder.ApplyConfiguration(new CompanyConfiguration());
            modelBuilder.ApplyConfiguration(new CompanyGroupConfiguration());
            modelBuilder.ApplyConfiguration(new CompanyUserConfiguration());

            base.OnModelCreating(modelBuilder);
        }
    }
}
