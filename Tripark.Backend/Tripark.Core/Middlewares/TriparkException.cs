﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Net;
using System.Threading.Tasks;
using Tripark.Domain.Common;
using Tripark.Utility.Helpers;

namespace Tripark.Core.Middlewares
{
    public class TriparkException
    {
        private readonly RequestDelegate _next;
        private readonly ILogger _logger;

        public TriparkException(RequestDelegate next, ILoggerFactory logFactory)
        {
            _next = next;

            _logger = logFactory.CreateLogger("Tripark Exception Middleware");
        }

        public async Task Invoke(HttpContext context) //, IElasticService elasticService
        {
            try
            {
                await _next(context);
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    _logger.LogError($"{TimeHelpers.GetTurkeyTime()} - An Exception Occured : {ex.InnerException.Message}");
                }
                else
                {
                    _logger.LogError($"{TimeHelpers.GetTurkeyTime()} - An Exception Occured : {ex.Message}");
                }

                await GenerateCommonException(context, ex); //elasticService
            }
        }

        private async Task GenerateCommonException(HttpContext context, Exception exception) //, IElasticService elasticService
        {
            var errorMessage = "error";
            var statusCode = HttpStatusCode.BadRequest;
            var exceptionType = exception.GetType();
            switch (exception)
            {
                case Exception e when exceptionType == typeof(UnauthorizedAccessException):
                    statusCode = HttpStatusCode.Unauthorized;
                    errorMessage = "Oturumunuz sonlandırıldı. Lütfen giriş yapınız";
                    break;

                case CustomApiException e when exceptionType == typeof(CustomApiException):
                    statusCode = (HttpStatusCode)Enum.Parse(typeof(HttpStatusCode), e.Code);
                    errorMessage = e.Message;
                    break;

                default:
                    statusCode = HttpStatusCode.InternalServerError;
                    errorMessage = "Bu işlem şu anda gerçekleştirilemiyor";
                    break;
            }

            //if (await elasticService.CheckStatus())
            //{
            //    await elasticService.AddExceptionLogAsync(exception);
            //}

            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)statusCode;
            context.Items.Add("exception", exception);
            context.Items.Add("exceptionMessage", errorMessage);
        }
    }
    public static class TriparkExceptionExtensions
    {
        public static IApplicationBuilder UseTriparkExceptionMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<TriparkException>();
        }
    }
}
