﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Serilog;
using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using Tripark.Domain.Common;
using Tripark.Utility.Helpers;
using Tripark.Utility.Interfaces;

namespace Tripark.Core.Middlewares
{
    public class ApiResponse
    {
        private readonly RequestDelegate _next;
        private static readonly ILogger _logger = Log.ForContext<ApiResponse>();
        private IConfigurationsHelper _configurationsHelper;

        public ApiResponse(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context,IConfigurationsHelper configurationsHelper)
        {
            this._configurationsHelper = configurationsHelper;
            _configurationsHelper.SetChannel();

            if (IsSwagger(context))
            {               
                await this._next(context);
            }
            else
            {
                var currentBody = context.Response.Body;
                var currentStatus = context.Response.StatusCode;

                string requestBodyText = string.Empty;

                if (context.Request.Method == HttpMethods.Put || context.Request.Method == HttpMethods.Post)
                {
                    context.Request.EnableBuffering();

                    requestBodyText = await new StreamReader(context.Request.Body).ReadToEndAsync();

                    context.Request.Body.Position = 0;
                }

                using (var memoryStream = new MemoryStream())
                {
                    context.Response.Body = memoryStream;
                    await _next(context);

                    context.Response.Body = currentBody;
                    context.Response.ContentType = "application/json";
                    memoryStream.Seek(0, SeekOrigin.Begin);

                    var readToEnd = new StreamReader(memoryStream).ReadToEnd();
                    object objResult = null;

                    if (readToEnd.ValidateJson())
                    {
                        objResult = JsonConvert.DeserializeObject(readToEnd);
                    }
                    else
                    {
                        objResult = readToEnd;
                    }

                    string errorMessage = string.Empty;
                    string correletionId = string.Empty;

                    if (context.Items["exception"] != null)
                    {
                        errorMessage = context.Items["exceptionMessage"].ToString();

                        objResult = null;

                        if (context.Items["channelExceptionStatusCode"] != null)
                        {
                            context.Response.StatusCode = (int)context.Items["channelExceptionStatusCode"];
                        }

                        InsertErrorLog(errorMessage, context);
                    }
                    else if (!string.IsNullOrEmpty(readToEnd))
                    {
                        try
                        {
                            dynamic jsonData = JsonConvert.DeserializeObject(readToEnd);

                            if (jsonData != null && jsonData.statusCode != null)
                            {
                                errorMessage = jsonData.message;
                                context.Response.StatusCode = (int)jsonData.statusCode;
                                objResult = null;
                            }
                        }
                        catch (Exception ex)
                        {
                            //Serilog for now..//Elastic future
                            InsertErrorLog(ex.Message, context, ex);
                        }
                    }

                    Console.WriteLine($"Path: {context.Request.Path} \t Method: {context.Request.Method} \t QueryString: {context.Request.QueryString.Value} \t Body: {requestBodyText}");

                    var result = CommonResponse.Create((HttpStatusCode)context.Response.StatusCode, objResult, errorMessage, correletionId);
                    var serializedResult = JsonConvert.SerializeObject(result, Formatting.Indented, new JsonSerializerSettings
                    {
                        ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                    });

                    await context.Response.WriteAsync(serializedResult);
                }
            }
        }

        private bool IsSwagger(HttpContext context)
        {
            return context.Request.Path.StartsWithSegments("/swagger");
        }

        private void InsertErrorLog(string message, HttpContext context = null, Exception ex = null)
        {
            _logger.Error(message);
        }
    }

    public static class ApiResponseExtensions
    {
        public static IApplicationBuilder UseApiResponseMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<ApiResponse>();
        }
    }
}
