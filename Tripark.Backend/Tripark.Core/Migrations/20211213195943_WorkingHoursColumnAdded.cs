﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Tripark.Core.Migrations
{
    public partial class WorkingHoursColumnAdded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "UserRole",
                keyColumn: "Id",
                keyValue: new Guid("07da16e3-91d3-4d5f-8d20-26bb5021986c"));

            migrationBuilder.DeleteData(
                table: "UserRole",
                keyColumn: "Id",
                keyValue: new Guid("e91451e6-f93a-48dc-9e21-e5765ab7e0ea"));

            migrationBuilder.AddColumn<string>(
                name: "WorkingHours",
                table: "Company",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "Company",
                keyColumn: "Id",
                keyValue: new Guid("016fe50e-f13e-429e-a502-7da96d6bd601"),
                column: "AddDate",
                value: new DateTime(2021, 12, 13, 22, 59, 42, 398, DateTimeKind.Local).AddTicks(2751));

            migrationBuilder.UpdateData(
                table: "CompanyGroup",
                keyColumn: "Id",
                keyValue: new Guid("01d4d52f-d403-4084-90b3-21972f9d6501"),
                column: "AddDate",
                value: new DateTime(2021, 12, 13, 22, 59, 42, 399, DateTimeKind.Local).AddTicks(3553));

            migrationBuilder.UpdateData(
                table: "CompanyUser",
                keyColumn: "Id",
                keyValue: new Guid("0136af1b-42b4-4d7f-b99a-f168c9287165"),
                column: "AddDate",
                value: new DateTime(2021, 12, 13, 22, 59, 42, 407, DateTimeKind.Local).AddTicks(7156));

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: new Guid("01660ef8-cb4a-42c2-a82e-2a0e099ad4ab"),
                column: "AddDate",
                value: new DateTime(2021, 12, 13, 22, 59, 42, 389, DateTimeKind.Local).AddTicks(6006));

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: new Guid("99d4d52f-d403-4084-90b3-21972f9d65f8"),
                column: "AddDate",
                value: new DateTime(2021, 12, 13, 22, 59, 42, 392, DateTimeKind.Local).AddTicks(9742));

            migrationBuilder.InsertData(
                table: "UserRole",
                columns: new[] { "Id", "AddDate", "RoleId", "UpdateDate", "UserId" },
                values: new object[,]
                {
                    { new Guid("2ae315c1-7576-428f-b4d2-e39e57d36e22"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 2, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new Guid("01660ef8-cb4a-42c2-a82e-2a0e099ad4ab") },
                    { new Guid("44888020-0bd5-43a1-b163-088feab46d5d"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 99, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new Guid("99d4d52f-d403-4084-90b3-21972f9d65f8") }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "UserRole",
                keyColumn: "Id",
                keyValue: new Guid("2ae315c1-7576-428f-b4d2-e39e57d36e22"));

            migrationBuilder.DeleteData(
                table: "UserRole",
                keyColumn: "Id",
                keyValue: new Guid("44888020-0bd5-43a1-b163-088feab46d5d"));

            migrationBuilder.DropColumn(
                name: "WorkingHours",
                table: "Company");

            migrationBuilder.UpdateData(
                table: "Company",
                keyColumn: "Id",
                keyValue: new Guid("016fe50e-f13e-429e-a502-7da96d6bd601"),
                column: "AddDate",
                value: new DateTime(2021, 11, 28, 1, 43, 9, 930, DateTimeKind.Local).AddTicks(1273));

            migrationBuilder.UpdateData(
                table: "CompanyGroup",
                keyColumn: "Id",
                keyValue: new Guid("01d4d52f-d403-4084-90b3-21972f9d6501"),
                column: "AddDate",
                value: new DateTime(2021, 11, 28, 1, 43, 9, 931, DateTimeKind.Local).AddTicks(941));

            migrationBuilder.UpdateData(
                table: "CompanyUser",
                keyColumn: "Id",
                keyValue: new Guid("0136af1b-42b4-4d7f-b99a-f168c9287165"),
                column: "AddDate",
                value: new DateTime(2021, 11, 28, 1, 43, 9, 936, DateTimeKind.Local).AddTicks(895));

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: new Guid("01660ef8-cb4a-42c2-a82e-2a0e099ad4ab"),
                column: "AddDate",
                value: new DateTime(2021, 11, 28, 1, 43, 9, 923, DateTimeKind.Local).AddTicks(8987));

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: new Guid("99d4d52f-d403-4084-90b3-21972f9d65f8"),
                column: "AddDate",
                value: new DateTime(2021, 11, 28, 1, 43, 9, 926, DateTimeKind.Local).AddTicks(190));

            migrationBuilder.InsertData(
                table: "UserRole",
                columns: new[] { "Id", "AddDate", "RoleId", "UpdateDate", "UserId" },
                values: new object[,]
                {
                    { new Guid("e91451e6-f93a-48dc-9e21-e5765ab7e0ea"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 2, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new Guid("01660ef8-cb4a-42c2-a82e-2a0e099ad4ab") },
                    { new Guid("07da16e3-91d3-4d5f-8d20-26bb5021986c"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 99, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new Guid("99d4d52f-d403-4084-90b3-21972f9d65f8") }
                });
        }
    }
}
