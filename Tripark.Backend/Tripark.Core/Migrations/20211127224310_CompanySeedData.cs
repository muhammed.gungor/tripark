﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Tripark.Core.Migrations
{
    public partial class CompanySeedData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CompanyUsers_Company_CompanyId",
                table: "CompanyUsers");

            migrationBuilder.DropForeignKey(
                name: "FK_CompanyUsers_User_UserId",
                table: "CompanyUsers");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CompanyUsers",
                table: "CompanyUsers");

            migrationBuilder.DeleteData(
                table: "UserRole",
                keyColumn: "Id",
                keyValue: new Guid("7544abff-0503-4a8c-a388-40dcc04a3352"));

            migrationBuilder.DeleteData(
                table: "UserRole",
                keyColumn: "Id",
                keyValue: new Guid("77818174-2a35-4679-b4bb-a6b93894e3a5"));

            migrationBuilder.RenameTable(
                name: "CompanyUsers",
                newName: "CompanyUser");

            migrationBuilder.RenameIndex(
                name: "IX_CompanyUsers_UserId",
                table: "CompanyUser",
                newName: "IX_CompanyUser_UserId");

            migrationBuilder.RenameIndex(
                name: "IX_CompanyUsers_CompanyId",
                table: "CompanyUser",
                newName: "IX_CompanyUser_CompanyId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CompanyUser",
                table: "CompanyUser",
                column: "Id");

            migrationBuilder.InsertData(
                table: "CompanyGroup",
                columns: new[] { "Id", "AddDate", "GroupTitle", "IsDeleted", "UpdateDate" },
                values: new object[] { new Guid("01d4d52f-d403-4084-90b3-21972f9d6501"), new DateTime(2021, 11, 28, 1, 43, 9, 931, DateTimeKind.Local).AddTicks(941), "TriPark Autopark Systems", false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) });

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: new Guid("01660ef8-cb4a-42c2-a82e-2a0e099ad4ab"),
                column: "AddDate",
                value: new DateTime(2021, 11, 28, 1, 43, 9, 923, DateTimeKind.Local).AddTicks(8987));

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: new Guid("99d4d52f-d403-4084-90b3-21972f9d65f8"),
                column: "AddDate",
                value: new DateTime(2021, 11, 28, 1, 43, 9, 926, DateTimeKind.Local).AddTicks(190));

            migrationBuilder.InsertData(
                table: "UserRole",
                columns: new[] { "Id", "AddDate", "RoleId", "UpdateDate", "UserId" },
                values: new object[,]
                {
                    { new Guid("e91451e6-f93a-48dc-9e21-e5765ab7e0ea"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 2, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new Guid("01660ef8-cb4a-42c2-a82e-2a0e099ad4ab") },
                    { new Guid("07da16e3-91d3-4d5f-8d20-26bb5021986c"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 99, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new Guid("99d4d52f-d403-4084-90b3-21972f9d65f8") }
                });

            migrationBuilder.InsertData(
                table: "Company",
                columns: new[] { "Id", "AddDate", "CityId", "CompanyGroupId", "CountyId", "Description", "FullAddress", "IsDeleted", "IsMasterBranch", "Latitude", "Logo", "Longitude", "Name", "PhoneNumber", "Title", "TypeId", "UniqueAddressId", "UpdateDate" },
                values: new object[] { new Guid("016fe50e-f13e-429e-a502-7da96d6bd601"), new DateTime(2021, 11, 28, 1, 43, 9, 930, DateTimeKind.Local).AddTicks(1273), 16, new Guid("01d4d52f-d403-4084-90b3-21972f9d6501"), 0, "Test User Company", "Karaman Mahallesi - Korfez Sokak - No:7/10", false, true, 40.213684999999998, "", 28.997748999999999, "Tripark Otopark", "05558105297", "Tri Otopark", 1, "6X7X+F5", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) });

            migrationBuilder.InsertData(
                table: "CompanyUser",
                columns: new[] { "Id", "AddDate", "CompanyId", "IsDeleted", "UpdateDate", "UserId" },
                values: new object[] { new Guid("0136af1b-42b4-4d7f-b99a-f168c9287165"), new DateTime(2021, 11, 28, 1, 43, 9, 936, DateTimeKind.Local).AddTicks(895), new Guid("016fe50e-f13e-429e-a502-7da96d6bd601"), false, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new Guid("01660ef8-cb4a-42c2-a82e-2a0e099ad4ab") });

            migrationBuilder.AddForeignKey(
                name: "FK_CompanyUser_Company",
                table: "CompanyUser",
                column: "CompanyId",
                principalTable: "Company",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CompanyUser_User",
                table: "CompanyUser",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CompanyUser_Company",
                table: "CompanyUser");

            migrationBuilder.DropForeignKey(
                name: "FK_CompanyUser_User",
                table: "CompanyUser");

            migrationBuilder.DropPrimaryKey(
                name: "PK_CompanyUser",
                table: "CompanyUser");

            migrationBuilder.DeleteData(
                table: "CompanyUser",
                keyColumn: "Id",
                keyValue: new Guid("0136af1b-42b4-4d7f-b99a-f168c9287165"));

            migrationBuilder.DeleteData(
                table: "UserRole",
                keyColumn: "Id",
                keyValue: new Guid("07da16e3-91d3-4d5f-8d20-26bb5021986c"));

            migrationBuilder.DeleteData(
                table: "UserRole",
                keyColumn: "Id",
                keyValue: new Guid("e91451e6-f93a-48dc-9e21-e5765ab7e0ea"));

            migrationBuilder.DeleteData(
                table: "Company",
                keyColumn: "Id",
                keyValue: new Guid("016fe50e-f13e-429e-a502-7da96d6bd601"));

            migrationBuilder.DeleteData(
                table: "CompanyGroup",
                keyColumn: "Id",
                keyValue: new Guid("01d4d52f-d403-4084-90b3-21972f9d6501"));

            migrationBuilder.RenameTable(
                name: "CompanyUser",
                newName: "CompanyUsers");

            migrationBuilder.RenameIndex(
                name: "IX_CompanyUser_UserId",
                table: "CompanyUsers",
                newName: "IX_CompanyUsers_UserId");

            migrationBuilder.RenameIndex(
                name: "IX_CompanyUser_CompanyId",
                table: "CompanyUsers",
                newName: "IX_CompanyUsers_CompanyId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CompanyUsers",
                table: "CompanyUsers",
                column: "Id");

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: new Guid("01660ef8-cb4a-42c2-a82e-2a0e099ad4ab"),
                column: "AddDate",
                value: new DateTime(2021, 11, 21, 22, 26, 30, 943, DateTimeKind.Local).AddTicks(6666));

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: new Guid("99d4d52f-d403-4084-90b3-21972f9d65f8"),
                column: "AddDate",
                value: new DateTime(2021, 11, 21, 22, 26, 30, 946, DateTimeKind.Local).AddTicks(1818));

            migrationBuilder.InsertData(
                table: "UserRole",
                columns: new[] { "Id", "AddDate", "RoleId", "UpdateDate", "UserId" },
                values: new object[,]
                {
                    { new Guid("77818174-2a35-4679-b4bb-a6b93894e3a5"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 2, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new Guid("01660ef8-cb4a-42c2-a82e-2a0e099ad4ab") },
                    { new Guid("7544abff-0503-4a8c-a388-40dcc04a3352"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), 99, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new Guid("99d4d52f-d403-4084-90b3-21972f9d65f8") }
                });

            migrationBuilder.AddForeignKey(
                name: "FK_CompanyUsers_Company_CompanyId",
                table: "CompanyUsers",
                column: "CompanyId",
                principalTable: "Company",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CompanyUsers_User_UserId",
                table: "CompanyUsers",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
