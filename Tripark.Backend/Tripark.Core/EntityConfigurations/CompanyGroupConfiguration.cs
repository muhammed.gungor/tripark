﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using Tripark.Domain.Entities;
using Tripark.Utility.Constans;

namespace Tripark.Core.EntityConfigurations
{
    public class CompanyGroupConfiguration : IEntityTypeConfiguration<CompanyGroup>
    {

        public void Configure(EntityTypeBuilder<CompanyGroup> builder)
        {
            builder.HasKey(c => c.Id);

            builder.HasData(new CompanyGroup { AddDate = DateTime.Now, GroupTitle = "TriPark Autopark Systems", Id = SeedData.CompanyGroupId, IsDeleted = false, UpdateDate = DateTime.MinValue });

            builder.ToTable("CompanyGroup");
        }
    }
}
