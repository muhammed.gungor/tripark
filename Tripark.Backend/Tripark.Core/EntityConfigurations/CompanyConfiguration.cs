﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using Tripark.Domain.Entities;
using Tripark.Utility.Constans;
using Tripark.Utility.Enums;

namespace Tripark.Core.EntityConfigurations
{
    public class CompanyConfiguration : IEntityTypeConfiguration<Company>
    {
        public void Configure(EntityTypeBuilder<Company> builder)
        {
            builder.HasKey(c => c.Id);
            builder.Property(c => c.Name).IsRequired();
            builder.Property(c => c.Title).IsRequired();
            builder.Property(c => c.PhoneNumber).IsRequired();
            builder.Property(c => c.TypeId).IsRequired();

            builder.HasData(new Company
            {
                AddDate = DateTime.Now,
                CityId = 16,
                CompanyGroupId = SeedData.CompanyGroupId,
                Description = "Test User Company",
                FullAddress = "Karaman Mahallesi - Korfez Sokak - No:7/10",
                Id = SeedData.CompanyId,
                IsDeleted = false,
                IsMasterBranch = true,
                Latitude = 40.213685,
                Longitude = 28.997749,
                Logo = "",
                Name = "Tripark Otopark",
                Title = "Tri Otopark",
                PhoneNumber = "05558105297",
                TypeId = (int)CompanyTypes.AUTOPARK,
                UniqueAddressId = "6X7X+F5",
                UpdateDate = DateTime.MinValue,
                CountyId = 0
            });

            builder.ToTable("Company");
        }
    }
}
