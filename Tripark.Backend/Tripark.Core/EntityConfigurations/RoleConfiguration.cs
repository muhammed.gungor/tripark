﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Tripark.Domain.Entities;
using Tripark.Utility.Enums;

namespace Tripark.Core.EntityConfigurations
{
    public class RoleConfiguration : IEntityTypeConfiguration<Role>
    {
        public void Configure(EntityTypeBuilder<Role> builder)
        {
            builder.HasKey(c => c.Id);
            builder.Property(c => c.Name).HasMaxLength(300);

            builder.HasData(new Role() { Id = (int)UserRoles.SYSTEM_ADMIN, Name = "SystemAdmin" });
            builder.HasData(new Role() { Id = (int)UserRoles.WEB_CLIENT_ADMIN, Name = "WebClientAdmin" });
            builder.HasData(new Role() { Id = (int)UserRoles.MOBILE_USER, Name = "MobileUser" });

            builder.ToTable("Role");
        }
    }
}
