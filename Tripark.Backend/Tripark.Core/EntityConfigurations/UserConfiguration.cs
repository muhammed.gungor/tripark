﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using Tripark.Domain.Entities;
using Tripark.Utility.Constans;
using Tripark.Utility.Security;

namespace Tripark.Core.EntityConfigurations
{
    public class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.HasKey(c => c.Id);
            builder.Property(c => c.Email).IsRequired(true);
            builder.Property(c => c.Password).IsRequired(true);

            var defaultPassword = EncryptionHelper.Encrypt("123456");

            builder.HasData(new User() { AddDate = DateTime.Now, Email = "test@tripark.com", Id = SeedData.TestUserId, IsDeleted = false, IsActive = true, GsmNumber = "+905558105297", Password = defaultPassword, UpdateDate = DateTime.MinValue, Name = "Tripark Test User" });
            builder.HasData(new User() { AddDate = DateTime.Now, Email = "admin@tripark.com", Id = SeedData.AdminUserId, IsDeleted = false, IsActive = true, GsmNumber = "+905558105297", Password = defaultPassword, UpdateDate = DateTime.MinValue, Name = "Tripark Admin User" });

            builder.ToTable("User");
        }
    }
}
