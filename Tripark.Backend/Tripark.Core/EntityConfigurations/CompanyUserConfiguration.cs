﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using Tripark.Domain.Entities;
using Tripark.Utility.Constans;

namespace Tripark.Core.EntityConfigurations
{
    public class CompanyUserConfiguration : IEntityTypeConfiguration<CompanyUser>
    {
        public void Configure(EntityTypeBuilder<CompanyUser> builder)
        {
            builder.HasKey(c => c.Id);

            builder.HasOne(c => c.Company)
                .WithMany(c => c.CompanyUsers)
                .HasForeignKey(c => c.CompanyId)
                .OnDelete(DeleteBehavior.Cascade)
                .HasConstraintName("FK_CompanyUser_Company");

            builder.HasOne(c => c.User)
                .WithMany(c => c.CompanyUsers)
                .HasForeignKey(c => c.UserId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_CompanyUser_User");

            builder.HasData(new CompanyUser
            {
                AddDate = DateTime.Now,
                CompanyId = SeedData.CompanyId,
                Id = SeedData.CompanyUserId,
                IsDeleted = false,
                UpdateDate = DateTime.MinValue,
                UserId = SeedData.TestUserId
            });

            builder.ToTable("CompanyUser");
        }
    }
}
