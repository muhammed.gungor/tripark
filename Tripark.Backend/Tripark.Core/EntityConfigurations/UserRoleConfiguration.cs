﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using Tripark.Domain.Entities;
using Tripark.Utility.Constans;
using Tripark.Utility.Enums;

namespace Tripark.Core.EntityConfigurations
{
    class UserRoleConfiguration : IEntityTypeConfiguration<UserRole>
    {
        public void Configure(EntityTypeBuilder<UserRole> builder)
        {
            builder.HasKey(c => c.Id);

            builder.HasData(new UserRole() { Id = Guid.NewGuid(), RoleId = (int)UserRoles.WEB_CLIENT_ADMIN, UserId = SeedData.TestUserId });
            builder.HasData(new UserRole() { Id = Guid.NewGuid(), RoleId = (int)UserRoles.SYSTEM_ADMIN, UserId = SeedData.AdminUserId});

            builder.ToTable("UserRole");
        }
    }
}