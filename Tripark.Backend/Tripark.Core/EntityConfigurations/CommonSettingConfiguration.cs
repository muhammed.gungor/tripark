﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Tripark.Domain.Entities;

namespace Tripark.Core.EntityConfigurations
{
    public class CommonSettingConfiguration : IEntityTypeConfiguration<CommonSetting>
    {
        public void Configure(EntityTypeBuilder<CommonSetting> builder)
        {
            builder.HasKey(c => c.Id);
            builder.Property(c => c.Name).IsRequired();
            builder.Property(c => c.Value).IsRequired();

            builder.ToTable("CommonSetting");
        }
    }
}
