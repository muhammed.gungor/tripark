﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tripark.Core.Context;
using Tripark.Core.Interfaces;
using Tripark.Domain.Dto;
using Tripark.Domain.Entities;

namespace Tripark.Core.Repositories
{
    public class CompanyRepository : BaseRepository<Company>, ICompanyRepository
    {
        private TriparkDatabaseContext _dbContext;
        public CompanyRepository(TriparkDatabaseContext Context) : base(Context)
        {
            _dbContext = Context;
        }

        public async Task<List<CompanyResponse>> GetUserCompaniesWithBranches(Guid userId)
        {
            var userCompanies = await _dbContext.CompanyUsers.Where(c => c.UserId.Equals(userId)).Include(c=>c.Company).ToListAsync();//.Include("Company").ToListAsync();

            var list = userCompanies.Select(c => new CompanyResponse
            {
                Id = c.Company.Id,
                PrettyAddress = c.Company.FullAddress,
                Title = c.Company.Title,
                IsMasterBranch = c.Company.IsMasterBranch,
                
            }).ToList();

            return list;
        }
    }
}
