﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Tripark.Core.Context;
using Tripark.Core.Interfaces;
using Tripark.Utility.Helpers;

namespace Tripark.Core.Repositories
{
    public class BaseRepository<TEntity> : IBaseRepository<TEntity> where TEntity : class
    {
        private TriparkDatabaseContext _context;
        private DbSet<TEntity> _dbSet;
        public BaseRepository(TriparkDatabaseContext Context)
        {
            _context = Context;
            _dbSet = _context.Set<TEntity>();
        }

        public async Task AddAsync(TEntity entity)
        {
            try
            {
                TEntity _entity = entity;

                if (_entity.GetType().GetProperty("AddDate") != null)
                {
                    _entity.GetType().GetProperty("AddDate").SetValue(_entity, TimeHelpers.GetTurkeyTime());
                }

                if (_entity.GetType().GetProperty("Id") != null)
                {
                    _entity.GetType().GetProperty("Id").SetValue(_entity, Guid.NewGuid());
                }

                await _dbSet.AddAsync(_entity);
            }
            catch (Exception)
            {
                await _dbSet.AddAsync(entity);
            }
        }

        public async Task AddRangeAsync(IEnumerable<TEntity> entities)
        {
            await _dbSet.AddRangeAsync(entities);
        }

        public IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate)
        {
            return _dbSet.Where(predicate);
        }

        public async Task<IEnumerable<TEntity>> GetAllAsync()
        {
            return await _dbSet.ToListAsync();
        }

        public async Task<bool> Exist(Expression<Func<TEntity, bool>> predicate)
        {
            return await _dbSet.AnyAsync(predicate);
        }

        public ValueTask<TEntity> GetByIdAsync(Guid id)
        {
            return _dbSet.FindAsync(id);
        }

        public void Remove(TEntity entity, bool forceRemove = false)
        {
            if (!forceRemove && entity.GetType().GetProperty("Deleted") != null)
            {
                TEntity _entity = entity;

                _entity.GetType().GetProperty("Deleted").SetValue(_entity, true);

                this.Update(_entity);
            }
            else
            {
                if (_context.Entry(entity).State == EntityState.Detached)
                {
                    _dbSet.Attach(entity);
                }
                _dbSet.Remove(entity);
            }
        }

        public void RemoveRange(IEnumerable<TEntity> entities)
        {
            _dbSet.RemoveRange(entities);
        }

        public Task<TEntity> SingleOrDefaultAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return _dbSet.SingleOrDefaultAsync(predicate);
        }

        public void Update(TEntity Entity)
        {
            _dbSet.Attach(Entity);
            _context.Entry(Entity).State = EntityState.Modified;
        }

        public async Task<TEntity> Get(Expression<Func<TEntity, bool>> filter, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy, string includeProperties)
        {
            IQueryable<TEntity> query = _dbSet;

            if (filter != null)
            {
                query = query.Where(filter);
            }
            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            if (orderBy != null)
            {
                return await orderBy(query).FirstOrDefaultAsync();
            }
            else
            {
                return await query.FirstOrDefaultAsync();
            }
        }

        public async Task<IEnumerable<TEntity>> GetMultiple(Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null, string includeProperties = "")
        {
            IQueryable<TEntity> query = _dbSet;

            if (filter != null)
            {
                query = query.Where(filter);
            }
            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            if (orderBy != null)
            {
                return await orderBy(query).ToListAsync();
            }
            else
            {
                return await query.ToListAsync();
            }
        }
    }

}
