﻿using System;
using System.Collections.Generic;
using System.Text;
using Tripark.Core.Context;
using Tripark.Core.Interfaces;
using Tripark.Domain.Entities;

namespace Tripark.Core.Repositories
{
    public class CompanyUserRepository : BaseRepository<CompanyUser>,ICompanyUserRepository
    {
        private TriparkDatabaseContext _dbContext;
        public CompanyUserRepository(TriparkDatabaseContext Context) : base(Context)
        {
            _dbContext = Context;
        }
    }
}
