﻿using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using Tripark.Core.Context;
using Tripark.Core.Interfaces;
using Tripark.Domain.Entities;
using Tripark.Utility.Enums;

namespace Tripark.Core.Repositories
{
    public class UserRepository :BaseRepository<User>, IUserRepository
    {
        private  TriparkDatabaseContext _dbContext;
        public UserRepository(TriparkDatabaseContext Context) : base(Context)
        {
            _dbContext = Context;
        }

        public async Task AddUserWithUserRole(User user,UserRoles userRole)
        {
            await base.AddAsync(user);

            var role = await _dbContext.Roles.Where(c=>c.Id.Equals(userRole)).FirstOrDefaultAsync();

            var insertableUserRole = new UserRole
            {
                UserId = user.Id,
                Role = role
            };

            await _dbContext.UserRoles.AddAsync(insertableUserRole);
        }
    }
}
