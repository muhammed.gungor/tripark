﻿using System;
using System.Collections.Generic;
using System.Text;
using Tripark.Domain.Entities;

namespace Tripark.Core.Interfaces
{
    public interface ICompanyUserRepository : IBaseRepository<CompanyUser>
    {

    }
}
