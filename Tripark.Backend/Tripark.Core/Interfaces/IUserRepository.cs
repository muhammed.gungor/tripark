﻿using System.Threading.Tasks;
using Tripark.Domain.Entities;
using Tripark.Utility.Enums;

namespace Tripark.Core.Interfaces
{
    public interface IUserRepository : IBaseRepository<User>
    {
        /// <summary>
        /// Adds user with own role to UserRole table
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        Task AddUserWithUserRole(User user, UserRoles userRole);
    }
}
