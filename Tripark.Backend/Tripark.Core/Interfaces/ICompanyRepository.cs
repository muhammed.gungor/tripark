﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tripark.Domain.Dto;
using Tripark.Domain.Entities;

namespace Tripark.Core.Interfaces
{
    public interface ICompanyRepository: IBaseRepository<Company>
    {
        /// <summary>
        /// Get user companies with company branches.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<List<CompanyResponse>> GetUserCompaniesWithBranches(Guid userId);
    }
}
