﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Tripark.Core.Context;
using Tripark.Core.Interfaces;
using Tripark.Core.Repositories;

namespace Tripark.Core.UnitofWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly TriparkDatabaseContext _dbContext;
        private UserRepository userRepository;
        private CompanyRepository companyRepository;
        private CompanyUserRepository companyUserRepository;

        public UnitOfWork(TriparkDatabaseContext databaseContext) => _dbContext = databaseContext;

        #region Repo Define

        public IUserRepository UserRepository => userRepository = userRepository ?? new UserRepository(_dbContext);
        
        public ICompanyRepository CompanyRepository => companyRepository = companyRepository ?? new CompanyRepository(_dbContext);
        
        public ICompanyUserRepository CompanyUserRepository => companyUserRepository = companyUserRepository ?? new CompanyUserRepository(_dbContext);

        #endregion

        #region Database Method Implementations

        public IBaseRepository<T> GetRepository<T>() where T : class
        {
            return new BaseRepository<T>(_dbContext);
        }

        public void Rollback()
        {
            _dbContext.ChangeTracker
                    .Entries()
                    .ToList()
                    .ForEach(x => x.Reload());
        }

        public async Task<int> SaveChangesAsync()
        {
            try
            {
                return await _dbContext.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception("Save Changes Ex.", ex.InnerException);
            }
        }

        #region IDisposable Support
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _dbContext.Dispose();
                }

                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
        #endregion

    }
}
