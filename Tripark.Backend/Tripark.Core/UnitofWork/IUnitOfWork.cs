﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tripark.Core.Interfaces;

namespace Tripark.Core.UnitofWork
{
    public interface IUnitOfWork : IDisposable
    {
        IBaseRepository<T> GetRepository<T>() where T : class;
        Task<int> SaveChangesAsync();
        void Rollback();

        #region Repository Implementations
        IUserRepository UserRepository { get; }
        ICompanyRepository CompanyRepository { get; }
        ICompanyUserRepository CompanyUserRepository { get; }
        #endregion
    }
}
