﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Tripark.Service.Interfaces;

namespace Tripark.API.Controllers
{
    [Route("api/[controller]")]
    public class TestController : Controller
    {
        private readonly ITestService _testService;

        public TestController(ITestService testService)
        {
            this._testService = testService;
        }

        [Route("test")]
        [HttpGet]
        public async Task<IActionResult> Test([FromQuery] string stringParameter)
        {
            return Ok(await _testService.ReturnStringVariable(stringParameter));
        }
    }
}
