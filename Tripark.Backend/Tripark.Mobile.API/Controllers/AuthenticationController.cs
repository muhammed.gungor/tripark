﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Tripark.Domain.Dto;
using Tripark.Service.Interfaces;
using Tripark.Utility.Attributes;

namespace Tripark.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {

        private readonly IAuthenticationService _authenticationService;

        public AuthenticationController(IAuthenticationService authenticationService)
        {
            _authenticationService = authenticationService;
        }

        [HttpPost]
        [Route("register")]
        [ProducesResponseType(typeof(RegisterRequest), 200)]
        [Produces("application/json")]
        [ServiceFilter(typeof(ChannelControl))]
        [AllowAnonymous]
        public async Task<IActionResult> RegisterAsync([FromBody] RegisterRequest registerRequest)
        {
            return Ok(await _authenticationService.RegisterAsync(registerRequest));
        }

        [ServiceFilter(typeof(ChannelControl))]
        [HttpPost]
        [Route("login")]
        [ProducesResponseType(typeof(MobileLoginResponse), 200)]
        [Produces("application/json")]
        [AllowAnonymous]
        public async Task<IActionResult> RegisterAsync([FromBody] LoginRequest loginRequest)
        {
            return Ok(await _authenticationService.LoginAsync(loginRequest));
        }
    }
}
