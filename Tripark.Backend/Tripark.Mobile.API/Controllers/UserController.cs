﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Tripark.Service.Interfaces;

namespace Tripark.Mobile.API.Controllers
{
    [Route("/api/[Controller]")]
    [ApiController]
    public class UserController : Controller
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }
        [HttpGet]
        public async Task<IActionResult> GetUser()
        {
            //var user = (User)HttpContext.Items["User"];
            return Ok(await _userService.GetUser(System.Guid.Parse("1d38ba49-1587-4bc5-b8a1-034825047bab")));
        }
    }
}
