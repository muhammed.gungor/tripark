﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Tripark.Domain.Models.CompanyBranch.Dto;
using Tripark.Service.Interfaces;

namespace Tripark.Mobile.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompanyController : ControllerBase
    {
        private readonly ICompanyService _companyService;

        public CompanyController(ICompanyService companyService)
        {
            _companyService = companyService;
        }

        [HttpGet]
        public async Task<IActionResult> Get(CompanyBranchMainRequest model)
        {
            return Ok(await _companyService.GetCarParksNearMe(model));
        }
    }
}
