﻿using AutoMapper;
using Tripark.Domain.Dto;
using Tripark.Domain.Entities;

namespace Tripark.API.Mapping
{
    public class TriparkMap : Profile
    {
        public TriparkMap()
        {
            CreateMap<User, UserDto>().ReverseMap();
            CreateMap<RegisterResponse, User>().ReverseMap();
        }
    }
}
