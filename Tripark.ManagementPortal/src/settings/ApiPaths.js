module.exports =  {
    LOGIN : "auth/login",
    REGISTER: "auth/register",
    BRANCH_LIST: "company/branch/list",
    COMPANY_LIST: "company/list",
    COMPANY_PROFILE: "company/profile",
    USER_LIST: "management/user/list"
}