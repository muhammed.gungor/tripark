export const HttpMethods = {
    GET:"GET",
    POST:"POST",
    PUT:"PUT",
    DELETE:"DELETE",
    PATCH:"PATCH",
    OPTION:"OPTION"
}

export const HttpStatusCodes = {
    OK:200,
    CREATED:201,
    BAD_REQUEST:400,
    UNAUTHORIZED:401,
    FORBIDDEN:403,
    NOT_FOUND:404,
    SERVER_ERROR:500,    
}