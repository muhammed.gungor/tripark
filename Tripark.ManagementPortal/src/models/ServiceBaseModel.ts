export default class ServiceBaseModel {    
    UseLoading : boolean = true
    Method: string ="GET"
    ApiPath : string = ""
    MoreHeaders : any
    QueryParameter : string = ""
    Body : any = null
    MustAuthenticate: boolean = true

}