import { observable, toJS, } from "mobx";
import ServiceBaseModel from "../models/ServiceBaseModel";
import ApiPaths from "../settings/ApiPaths";
import { HttpMethods } from "../settings/HttpUtils";
import ServiceBase from "./ServiceBase";

class UserService {
    constructor() {
        this.listOfUsers = this.listOfUsers.bind(this)
    }

    @observable
    users: any = []

    async listOfUsers() {
        var model = new ServiceBaseModel();
        model.Method = HttpMethods.GET
        model.ApiPath = ApiPaths.USER_LIST

        return ServiceBase.connect(model)
            .then(() => {
                console.log("Api Response from List Users : ", toJS(ServiceBase.response))
                this.users = ServiceBase.response;
            })
    }
}

export default observable(new UserService());