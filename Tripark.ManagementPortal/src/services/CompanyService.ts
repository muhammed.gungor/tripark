import { makeObservable, observable, toJS, } from "mobx";
import ServiceBaseModel from "../models/ServiceBaseModel";
import ApiPaths from "../settings/ApiPaths";
import { HttpMethods } from "../settings/HttpUtils";
import ServiceBase from "./ServiceBase";

class CompanyService {
    constructor() {
        this.listOfBranches = this.listOfBranches.bind(this)
    }

    @observable
    branches: any = []

    @observable
    companies: any = []

    @observable
    profile: any = {}

    async listOfBranches() {
        var model = new ServiceBaseModel();
        model.Method = HttpMethods.GET
        model.ApiPath = ApiPaths.BRANCH_LIST

        return ServiceBase.connect(model)
            .then(() => {
                console.log("Api Response from List branch : ", toJS(ServiceBase.response))
                this.branches = ServiceBase.response;
            })
    }

    async listOfCompanies() {
        var model = new ServiceBaseModel();
        model.Method = HttpMethods.GET
        model.ApiPath = ApiPaths.COMPANY_LIST

        return ServiceBase.connect(model)
            .then(() => {
                console.log("Api Response from List company : ", toJS(ServiceBase.response))
                this.companies = ServiceBase.response;
            })
    }

    async getCompanyProfile(companyId: string) {
        var model = new ServiceBaseModel();
        model.Method = HttpMethods.GET
        model.ApiPath = ApiPaths.COMPANY_PROFILE
        model.QueryParameter = companyId

        return ServiceBase.connect(model)
            .then(() => {
                console.log("Api Response from Profile company : ", toJS(ServiceBase.response))
                this.profile = ServiceBase.response;
            })
    }

    async updateCompanyProfile(contract:any){
        var model = new ServiceBaseModel();
        model.Method = HttpMethods.PUT
        model.ApiPath = ApiPaths.COMPANY_PROFILE
        model.Body = contract

        return ServiceBase.connect(model).then(() => {
            console.log("Api Response from Update Profile company : ", toJS(ServiceBase.response))
            this.profile = ServiceBase.response
        })
    }
}

export default observable(new CompanyService());