import { observable } from 'mobx';
import React from 'react';
import { useCookies } from 'react-cookie';
import { hideLoading, showLoading, showMessage } from '../commons/Helpers';
import ServiceBaseModel from '../models/ServiceBaseModel';
import { HttpStatusCodes } from '../settings/HttpUtils';
import AuthenticationService from './AuthenticationService';
import { useHistory } from 'react-router-dom';
var configs = require("../settings/SystemConfig");

class ServiceBase extends React.Component<any, any> {

    @observable
    response = {
        success: false,
        data: null,
        loading: false,
        error: null,
        statusCode: null
    }

    async connect(model: ServiceBaseModel) {
        if (model.UseLoading) {
            this.response.loading = true;
        }

        if (model.MustAuthenticate) {
        }

        showLoading();

        console.log("ServiceBase Authentication token:", model.ApiPath, "---", AuthenticationService.getUserToken())

        return fetch(
            configs.BASE_API_URL + model.ApiPath + (model.QueryParameter ? "/" + model.QueryParameter : ""),
            {
                headers: {
                    "Content-Type": "application/json",
                    "Accept-Language": "en",
                    "Authorization": "Bearer " + AuthenticationService.getUserToken()
                },
                body: model.Body ? JSON.stringify(model.Body) : null,
                method: model.Method
            }
        ).then((data) => {
            return data.json()
        }).then((data) => {
            this.response.statusCode = data.statusCode;

            if (data.statusCode === HttpStatusCodes.OK) {
                console.log("Success data,", data.result);
                this.response.success = true;
                this.response.data = data.result;
                this.response.error = null;
            }
            else if (data.statusCode === HttpStatusCodes.UNAUTHORIZED) {
                this.response.error = data.errorMessage;
                this.response.success = false;

                document.cookie = "loginData=; expires=Thu, 01 Jan 1970 00:00:01 GMT;";
                window.location.assign("/signin")
            }
            else {
                this.response.error = data.errorMessage;
                this.response.success = false;
                console.log("Error message : ", data);
                showMessage("error", data.errorMessage);
            }

            this.response.loading = false;
            hideLoading();
        }).catch(error => {
            this.response.loading = false;
            this.response.success = false;
            this.response.error = error.message;
            console.log("Service request failed with an exception.", error);
            hideLoading();
            showMessage("error", "The service does not working right now.");
        })
    }
}

export default observable(new ServiceBase(this));
