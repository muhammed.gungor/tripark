import {  observable, toJS, } from "mobx";
import ServiceBaseModel from "../models/ServiceBaseModel";
import ApiPaths from "../settings/ApiPaths";
import { HttpMethods } from "../settings/HttpUtils";
import ServiceBase from "./ServiceBase";

class AuthenticationService {
    constructor() {
        this.loginAsync = this.loginAsync.bind(this)
    }

    model: ServiceBaseModel = new ServiceBaseModel()

    @observable
    login: any = {}

    @observable
    userToken: string = ""

    async loginAsync(loginModel: any) {
        this.model.Method = HttpMethods.POST
        this.model.ApiPath = ApiPaths.LOGIN
        this.model.Body = loginModel
        this.model.MustAuthenticate = false;

        return ServiceBase.connect(this.model)
            .then(() => {
                console.log("Login response data:", toJS(ServiceBase.response.data))

                this.login = ServiceBase.response
                this.userToken = this.login.data.token
            })
    }

    getUserToken() {
        if (this.login && this.login.data && this.login.data.token) {
            return this.login.data.token;
        }
        else {
            this.login = {}
        }
    }
}

export default observable(new AuthenticationService());