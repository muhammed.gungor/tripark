export const enum AuthenticationStatus {
    AUTHORIZED = 1,
    UNAUTHORIZED = 0 
}