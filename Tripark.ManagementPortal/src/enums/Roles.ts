export const enum Roles {
    SYSTEM_ADMIN = 99,
    WEB_CLIENT_ADMIN = 2,
    GENERAL_USER = 100
}