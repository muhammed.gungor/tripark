import React from 'react';
import { notification, message } from 'antd';
import { LoadingOutlined } from '@ant-design/icons';

export var loadingVisible = false;

export const showLoading = () => {

    if (!loadingVisible) {
        loadingVisible = message.open({
            content: <div><LoadingOutlined /> Please wait.. </div>,
            duration: 0
        })
    }
};

export const hideLoading = () => {
    setTimeout(() => {
        if (loadingVisible) {
            loadingVisible();
            loadingVisible = false;
        }
    }, 200);
}

export const showMessage = (type, message) => {
    notification.config({
        placement: "topRight"
    });

    notification["" + type]({
        message: "" + message,
        style: {
            width: 380,
            marginLeft: 'auto',
        }
    })
}
