
export const WorkingDays = [
    {
        "key": "Monday",
        "value": "MONDAY"
    },
    {
        "key": "Tuesday",
        "value": "TUESDAY"
    },
    {
        "key": "Wednesday",
        "value": "WEDNESDAY"
    },
    {
        "key": "Thursday",
        "value": "THURSDAY"
    },
    {
        "key": "Friday",
        "value": "FRIDAY"
    },
    {
        "key": "Saturday",
        "value": "SATURDAY"
    },
    {
        "key": "Sunday",
        "value": "SUNDAY"
    }
]