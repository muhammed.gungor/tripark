import React from 'react';
//import logo from './logo.svg';
import './App.css';
import { CookiesProvider } from 'react-cookie';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { Skeleton } from 'antd';
import 'antd/dist/antd.css'; 

const MainLayout = React.lazy(() => import("../src/views/Layouts/MainLayout"));
const LandingLayout = React.lazy(() => import("../src/views/Layouts/LandingLayout"));
const SystemAdminLayout = React.lazy(() => import("../src/views/Layouts/AdminLayout"));

const loadingFallback = () => <div style={{ display: 'flex', justifyContent: 'center', padding: 20 }}><div style={{ width: 560 }}><Skeleton avatar paragraph={{ rows: 4 }} /></div></div>;

function App() {
  return (
    <CookiesProvider>
      <BrowserRouter>
        <React.Suspense fallback={loadingFallback()}>
          <Switch>
            <Route path="/main" name="MainLayout" render={props => <MainLayout {...props} />}/>
            <Route path="/admin" name="AdminLayout" render={props => <SystemAdminLayout {...props} />}/>
            <Route path="/" name="LandingPage" render={props => <LandingLayout {...props} />}/>
          </Switch>
        </React.Suspense>
      </BrowserRouter>
    </CookiesProvider>
  );
}

export default App;
