import React, { useEffect, useState } from "react";
import { observer } from "mobx-react-lite";
import { Skeleton,Typography, Tag } from "antd";
import { Table } from 'antd';
import UserService from "../../../../services/UserService";

const { Text } = Typography;
const List = observer(() => {

    const [users, setUsers] = useState([]); 

    useEffect(() => {
        UserService.listOfUsers().then(() => {
            console.log("User List then : ", UserService.users);
            if (UserService.users.success) {
                console.log("user list success:", UserService.users.data);
                setUsers(UserService.users.data)
            }
        });
    }, [])

    useEffect(() => {

    },[users]);

    const columns = [
        {
            title: 'Name',
            dataIndex: 'name',
            key: 'name'
        },
        {
            title: 'Surname',
            dataIndex: 'lastname',
            key: 'lastname'
        },
        {
            title: 'Is Active',
            dataIndex: 'isActive',
            key: 'isActive' ,
            render: (record) => (
                record ? <Tag color="green">Active</Tag> : <Tag color="green">Passive</Tag>
            )
        },
        {
            title: 'Phone Number',
            dataIndex: 'gsmNumber',
            key: 'gsmNumber'
        },
        {
            title: 'Registration Date',
            dataIndex: 'addDate',
            key: 'addDate'
        },
        {
            title: 'Roles',
            dataIndex: 'userRoles',
            key: 'userRoles',
            render: (record) => (
                <Text style={{ fontSize: 14 }}>
                    {record.map(item => {
                        return <span>{item}</span>;
                    })}
                </Text>
            )
        }
    ];

    return (UserService.users && UserService.users.loading) ?
        <Skeleton></Skeleton> :
        <Table dataSource={users} columns={columns} style={{ width: "100%", height: "inherit" }} />
});


export default List;