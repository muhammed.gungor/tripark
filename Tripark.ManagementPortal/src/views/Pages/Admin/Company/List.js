import React, { useEffect, useState } from "react";
import { observer } from "mobx-react-lite";
import CompanyService from "../../../../services/CompanyService";
import { Skeleton } from "antd";
import { Table } from 'antd';

const List = observer(() => {
    const [companies, setCompanies] = useState([]);
    
    useEffect(() => {
        CompanyService.listOfCompanies().then(() => {
            console.log("Company List then : ", CompanyService.companies);
            if (CompanyService.companies.success) {
                console.log("sucess:",CompanyService.companies);
                setCompanies(CompanyService.companies.data)
            }
        });
    }, [])

    useEffect(() => {

    },[companies]);

    const columns = [
        {
            title: 'Title',
            dataIndex: 'title',
            key: 'title'
        },
        {
            title: 'Phone',
            dataIndex: 'phone',
            key: 'phone'
        },
        {
            title: 'Is Active',
            dataIndex: 'isActive',
            key: 'isActive'
        }
    ];

    return (CompanyService.companies && CompanyService.companies.loading) ?
        <Skeleton></Skeleton> :
        <Table dataSource={companies} columns={columns} style={{ width:"100%", height:"inherit"}}/>
});


export default List;