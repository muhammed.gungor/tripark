import React, { useEffect } from "react";
import { observer } from "mobx-react";
import { Form, Input, Button, Checkbox, Layout, Card } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import { Container } from 'reactstrap';
import { LoginOutlined } from '@ant-design/icons';
import AuthenticationService from "../../../services/AuthenticationService";
import { useHistory } from 'react-router-dom';
import { useCookies } from "react-cookie";

const { Content } = Layout;

const SignIn = observer(() => {

    const [form] = Form.useForm();
    const history = useHistory();
    const [cookie, setCookie, removeCookie] = useCookies(["loginData","selectedCompany"]);

    const onFinish = (values) => {
        form.validateFields(['email', 'password']).then((err, reje) => {
            var contract = {
                email: values.email,
                password: values.password
            };

            AuthenticationService.loginAsync(contract).then(() => {
                if (AuthenticationService.login.success) {
                    if (AuthenticationService.login.baseUrl !== null) {
                        console.log("AuthenticationService.login.baseUrl:", AuthenticationService.login.data.baseUrl)
                        
                        removeCookie(["loginData","selectedCompany"]);
                        setCookie("loginData", JSON.stringify(AuthenticationService.login), { path: "/" })
                        history.push("" + AuthenticationService.login.data.baseUrl)
                    }
                } else {

                }
            });
        })
    };

    useEffect(() => {
        console.log("useEffect.. didmount.")
    });

    const formItemLayout = {
        labelCol: {
            xs: { span: 24 },
            sm: { span: 8 },
        },
        wrapperCol: {
            xs: { span: 24 },
            sm: { span: 24 },
        },
    };

    const tailFormItemLayout = {
        wrapperCol: {
            xs: {
                span: 24,
                offset: 0,
            },
            sm: {
                span: 24,
                offset: 0,
            },
        },
    };

    return <Container style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
        <Content style={{ maxWidth: 360, display: 'flex', flexDirection: 'column', paddingTop: 32 }}>
            <Card style={{ maxWidth: 360, display: 'flex', flexDirection: 'column', paddingTop: 32, backgroundColor: "#eee0da40", borderColor: "#eee0da40" }} >
                <Form  {...formItemLayout}
                    name="normal_login"
                    className="login-form"
                    form={form}
                    initialValues={{ remember: true }}
                    onFinish={onFinish}
                >
                    <Form.Item
                        name="email"
                        rules={[{ required: true, message: 'Please input your Username!', type: 'email' }]}
                    >
                        <Input prefix={<UserOutlined style={{ color: "#cd4532" }} />} placeholder="Username" />
                    </Form.Item>
                    <Form.Item
                        name="password"
                        rules={[{ required: true, message: 'Please input your Password!' }]}
                    >
                        <Input
                            prefix={<LockOutlined style={{ color: "#cd4532" }} />}
                            type="password"
                            placeholder="Password"
                        />
                    </Form.Item>
                    <Form.Item>
                        <Form.Item name="remember" valuePropName="checked" noStyle>
                            <Checkbox>Remember me</Checkbox>
                        </Form.Item>

                        <a className="login-form-forgot" href="/register">
                            Forgot password
                        </a>
                    </Form.Item>

                    <Form.Item {...tailFormItemLayout} style={{ display: "flex", flexDirection: "column", justifyContent: "center", alignItems: "center" }}>
                        <Button type="default" htmlType="submit" style={{ width: 300 }} icon={<LoginOutlined />}>
                            Log in
                        </Button>
                    </Form.Item>
                </Form>
            </Card>
        </Content>
    </Container>
});

export default SignIn;