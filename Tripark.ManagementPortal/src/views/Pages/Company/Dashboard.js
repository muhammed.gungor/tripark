import React, { useEffect, useState } from "react";
import { observer } from "mobx-react-lite";
import CompanyService from "../../../services/CompanyService";
import { Avatar, Card, Skeleton } from "antd";
import { useCookies } from "react-cookie";
import { useHistory } from "react-router";
import Meta from "antd/lib/card/Meta";
import { Content } from "antd/lib/layout/layout";

const CompanyProfile = observer(() => {

    const [cookie, setCookie] = useCookies();
    const history = useHistory();
    const [company, setCompany] = useState(null);

    useEffect(() => {
        console.log("Dashboard loaded.");
        CompanyService.listOfBranches().then(() => {
            // console.log("Company Profile then() : ", CompanyService.profile);
            // if (CompanyService.profile.success) {
            //     console.log("success profile:", CompanyService.profile);
            // }
        });
    }, [])

    return (CompanyService.profile && CompanyService.profile.loading) ?
        <Skeleton></Skeleton> :
        <Content>
            <Card>

            </Card>
        </Content>

});


export default CompanyProfile;