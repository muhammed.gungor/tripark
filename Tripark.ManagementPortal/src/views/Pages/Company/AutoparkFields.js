import React, { useEffect, useState } from "react";
import { observer } from "mobx-react-lite";
import CompanyService from "../../../services/CompanyService";
import { Avatar, Button, Card, Col, Divider, Form, Input, Row, Skeleton } from "antd";
import { useCookies } from "react-cookie";
import { useHistory } from "react-router";
import Meta from "antd/lib/card/Meta";
import { SaveOutlined } from "@ant-design/icons";
import Colors from "../../../commons/Colors";
import { showMessage } from "../../../commons/Helpers";


const AutoparkFields = observer(() => {

    const [cookie, setCookie] = useCookies();
    const history = useHistory();
    const [company, setCompany] = useState({});
    const [form] = Form.useForm();

    useEffect(() => {
        let isMounted = false;

        var currentCompany = cookie["selectedCompany"];

        if (currentCompany && currentCompany !== undefined) {
            if (isMounted)
                return;
                
            CompanyService.getCompanyProfile(currentCompany.id).then(() => {
                if (CompanyService.profile.success) {
                    console.log("success profile data:", CompanyService.profile.data);
                    setCompany(CompanyService.profile.data)
                    form.setFieldsValue({ ...CompanyService.profile.data })
                }
            });
        }
        else {
            history.push("/main");
        }

        return () => {
            isMounted = true;
        }
    }, []);

    const onFinish = (values) => {
        form.validateFields(['name', 'phoneNumber']).then((err, reje) => {
            console.log("worked", values)

            var contract = {
                name: values.name,
                phoneNumber: values.phoneNumber,
                description: values.description,
                id: company.id,
                logo: company.logo
            }

            CompanyService.updateCompanyProfile(contract).then(() => {
                console.log("success profile update data:", CompanyService.profile.data);
                if (CompanyService.profile.success) {
                    showMessage("success", "Güncelleme işlemi başarılı")
                }
            })
        })
    };

    const layout = {
        labelCol: { span: 8 },
        wrapperCol: { span: 16 },
    };

    return (CompanyService.profile === {} || CompanyService.profile.loading) ?
        <Skeleton></Skeleton> :
        <Card>
            <div className="flex flexDirectionRow justifyCenter alignItemsCenter">
                <Meta
                    avatar={<Avatar src={company.logo} style={{ width: 120, height: 120, marginLeft: 16 }} />}
                />
            </div>

            <Divider orientation="center">{company.title}</Divider>

            <Form {...layout} requiredMark={false} className="flex flexDirectionColumn justifyCenter"
                onFinish={onFinish}
                form={form}>
                <Row gutter={32} className="flex flexDirectionRow justifyCenter">
                    <Col xs={24} md={8}>
                        <Form.Item
                            name="name"
                            label="Company Name"
                            rules={[{ required: true, message: "Please input Company Name" }]}>
                            <Input />
                        </Form.Item>
                    </Col>
                </Row>
                <Row gutter={32} className="flex flexDirectionRow justifyCenter">
                    <Col xs={24} md={8}>
                        <Form.Item
                            name="phoneNumber"
                            label="Phone Number"
                            rules={[{ required: true, message: 'Please input your Phone Number!' }]}
                        >
                            <Input />
                        </Form.Item>
                    </Col>
                </Row>
                <Row gutter={32} className="flex flexDirectionRow justifyCenter">
                    <Col xs={24} md={8}>
                        <Form.Item
                            name="description"
                            label="Description"
                        >
                            <Input.TextArea />
                        </Form.Item>
                    </Col>
                </Row>
                <Row className="flex flexDirectionRow justifyCenter">
                    <Button type="primary" htmlType="submit" style={{ width: 120, backgroundColor: Colors.tripark, borderColor: Colors.gray[100] }} icon={<SaveOutlined />}>
                        Save
                    </Button>
                </Row>
            </Form>
        </Card>
})


export default AutoparkFields;