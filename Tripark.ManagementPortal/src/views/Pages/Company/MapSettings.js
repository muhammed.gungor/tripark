import React, { useEffect, useState } from "react";
import { observer } from "mobx-react-lite";
import { Card, Skeleton } from "antd";
import L from 'leaflet';
import { MapContainer, Marker, Popup, TileLayer, useMapEvents } from "react-leaflet";
import CompanyService from "../../../services/CompanyService";
import 'leaflet/dist/leaflet.css';
import icon from 'leaflet/dist/images/marker-icon.png';
import iconShadow from 'leaflet/dist/images/marker-shadow.png';

const MapSettings = observer(() => {
    const[companyPosition, setCompanyPosition] = useState([51.505, -0.09])

    useEffect(() => {
        setTimeout(() => {
            setCompanyPosition([71.505, 25.5])
        }, 3000);
    }, []);

    // const map = useMapEvents(
    //     map.flyTo(e.latlng, map.getZoom())
    // )

    const position = companyPosition
    let DefaultIcon = L.icon({
        iconUrl: icon,
        shadowUrl: iconShadow
    });
    
    function LocationMarker() {
        const [position, setPosition] = useState(null)
        const map = useMapEvents({
          click() {
            map.locate()
          },
          locationfound(e) {
            setPosition(e.latlng)
            map.flyTo(e.latlng, map.getZoom())
          },
        })
      
        return position === null ? null : (
          <Marker position={position}>
            <Popup>You are here</Popup>
          </Marker>
        )
      }

    L.Marker.prototype.options.icon = DefaultIcon;

    return (CompanyService.companies && CompanyService.companies.loading) || false ?
        <Skeleton></Skeleton> :
        //style={{maxWidth:850,maxHeight:400}}
        <div>
            <Card style={{marginBottom:12}}>

            </Card>
            <Card>
                <MapContainer center={position} zoom={13} scrollWheelZoom={false} >
                    <TileLayer
                        attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                    />
                   <LocationMarker />
                </MapContainer>
            </Card>
        </div>
});


export default MapSettings;