import React, { useEffect, useState } from "react";
import { observer } from "mobx-react-lite";
import { Skeleton, Typography, Tag } from "antd";
import { Table } from 'antd';
import UserService from "../../../services/UserService";
import { WorkingDays } from "../../../commons/StaticVariables"

const { Text } = Typography;
const List = observer(() => {

    const [workingHours, setWorkingHours] = useState([]);

    useEffect(() => {
        UserService.listOfUsers().then(() => {
            console.log("User List then : ", UserService.users);
            if (UserService.users.success) {
                var wh = setDefaultData();
                setWorkingHours(wh)
            }
        });
    }, [])

    useEffect(() => {

    }, [workingHours]);

    const setDefaultData = () => {
        let data = [];

        for (let i = 0; i < 7; i++) {
            data.push({
                id: i,
                key: WorkingDays[i].key,
                day: WorkingDays[i].value,
                startHour: null,
                endHour: null,
                isAllDay: true
            });
        }
        console.log("defaultdata:", data);
        return data;
    }

    const columns = [
        {
            title: 'Name',
            dataIndex: 'id',
            key: 'id'
        },
        {
            title: 'Surname',
            dataIndex: 'day',
            key: 'day'
        },
        // {
        //     title: 'Is Active',
        //     dataIndex: 'isActive',
        //     key: 'isActive',
        //     render: (record) => (
        //         record ? <Tag color="green">Active</Tag> : <Tag color="green">Passive</Tag>
        //     )
        // }
    ];

    return (UserService.users && UserService.users.loading) ?
        <Skeleton></Skeleton> :
        <Table dataSource={workingHours} columns={columns} style={{ width: "100%", height: "inherit" }} />
});


export default List;