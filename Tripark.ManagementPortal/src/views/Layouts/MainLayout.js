import React, { Suspense, useEffect, useState } from 'react';
import {
    Layout, Menu, Skeleton, Button, Avatar, Popover, Dropdown
} from 'antd';
import 'antd/dist/antd.css';
import { Redirect, Route, Switch } from 'react-router-dom';
import { useCookies } from 'react-cookie';
import { Container } from 'reactstrap';
import { observer } from 'mobx-react';
import CompanyService from '../../services/CompanyService';
import AuthenticationService from '../../services/AuthenticationService';
import { showMessage } from '../../commons/Helpers';
import Colors from '../../commons/Colors';
import { LogoutOutlined, UserOutlined, DashboardFilled, CarFilled, ImportOutlined, PaperClipOutlined, QuestionCircleFilled, DownOutlined, CarOutlined, RightSquareTwoTone, LeftSquareTwoTone, SettingFilled, GlobalOutlined, AppstoreOutlined, UpSquareFilled, PlusCircleTwoTone, FieldTimeOutlined } from '@ant-design/icons';
import { useHistory } from 'react-router-dom';
import ServiceBase from '../../services/ServiceBase';
import { HttpStatusCodes } from '../../settings/HttpUtils';
import routes from "../Layouts/Routes"
import { Roles } from '../../enums/Roles';
import { toJS } from 'mobx';

const { SubMenu } = Menu;
const { Header, Sider, Content } = Layout;

const menu = [
    {
        "languageKey": "MENU_MESSAGES",
        "icon": <DashboardFilled />,
        "key": "contacts",
        "url": "dashboard",
        "defaultValue": "Dashboard"
    },
    {
        "languageKey": "MENU_MY_COMPANY",
        "key": "settings",
        "icon": <CarFilled />,
        "items": [
            {
                "languageKey": "MENU_COMPANY_PROFILE",
                "key": "company-profile",
                "url": "company/profile",
                "defaultValue": "Company Profile",
                "icon": <SettingFilled />
            },
            {
                "languageKey": "MENU_MAP_SETTINGS",
                "key": "map-settings",
                "url": "company/map-settings",
                "defaultValue": "Map Settings",
                "icon": <GlobalOutlined />
            },
            {
                "languageKey": "MENU_FIELD_SETTINGS",
                "key": "autopark-fields",
                "url": "company/autopark-fields",
                "defaultValue": "Set Fields",
                "icon": <AppstoreOutlined />
            },
            {
                "languageKey": "MENU_WORKING_HOURS",
                "key": "working-hours",
                "url": "company/working-hours",
                "defaultValue": "Working Hours",
                "icon": <FieldTimeOutlined />
            }
        ],
        "defaultValue": "My AutoPark"
    },
    {
        "languageKey": "MENU_DEVELOPERS",
        "icon": <ImportOutlined />,
        "key": "developers",
        "url": "entries-exits",
        "defaultValue": "Entries & Exits"
    },
    {
        "languageKey": "MENU_AGREEMENTS",
        "icon": <PaperClipOutlined />,
        "key": "agreements",
        "url": "reports",
        "defaultValue": "Reports"
    },
    {
        "languageKey": "MENU_AGREEMENTS",
        "icon": <QuestionCircleFilled />,
        "key": "help",
        "url": "help",
        "defaultValue": "Help"
    }
];

const MainLayout = observer(() => {
    const [constructorHasRun, setConstructorHasRun] = useState(false);
    const [unauthenticated, setUnauthenticated] = useState(false);
    const [collapsed, setCollapsed] = useState(false);
    const [cookie, setCookie] = useCookies();
    const [selectedCompany, setSelectedCompany] = useState(null)
    const [branchList, setBranchList] = useState([]);
    const history = useHistory();

    const signOut = () => {
        setCookie("loginData", null, { path: "/" });
        setCookie("selectedCompany", null, { path: "/" });
        history.push('/')
    }

    const constructor = () => {
        if (constructorHasRun) return;

        console.log("Main Layout constructor");

        AuthenticationService.login = cookie["loginData"];
        console.log("AuthenticationService.login after assign:,", AuthenticationService.login);

        if (AuthenticationService.login === undefined || AuthenticationService.login.data === undefined || AuthenticationService.userToken === null || ServiceBase.response.statusCode === HttpStatusCodes.UNAUTHORIZED) {
            setUnauthenticated(true);
            console.log("SESSION EXPIRED.")
            showMessage("warning", "Your session has been expired. Please sign in again.")
            signOut();
        }
        else {
            setUnauthenticated(false);
        }

        setConstructorHasRun(true);
    };

    constructor();

    useEffect(() => {
        let isMounted = false;
        console.log("ilk useeffect")
        if (!unauthenticated) {
            CompanyService.listOfBranches().then(() => {
                if (isMounted) {
                    return;
                }

                if (CompanyService.branches.success) {
                    if (CompanyService.branches && CompanyService.branches.data.length > 0) {
                        var company = toJS(CompanyService.branches.data.find(c => c.isMasterBranch));
                        let list = CompanyService.branches.data;
                        setBranchList(list)
                    } else {
                        console.log("This user must be create a company")
                    }
                }
                else if (ServiceBase.response.statusCode === HttpStatusCodes.UNAUTHORIZED) {
                    setUnauthenticated(true);
                }
            })
        }

        return () => {
            isMounted = true;
        }

    }, []);

    useEffect(() => {
        console.log("test in second effect:", branchList)
        var company = branchList.find(c => c.isMasterBranch);
        if (company !== undefined) {
            setCompany(company.id);
        }
    }, [branchList])

    const toggle = () => {
        setCollapsed(!collapsed)
    };

    const goDashboard = () => {
        history.push('/main')
    }

    const menuRedirect = (menuUrl) => {
        history.push('/main/' + menuUrl);
    }

    const setCompany = (key) => {
        var company = branchList.find(c => c.id === key);
        setSelectedCompany(company);
        setCookie("selectedCompany", company, { path: "/" });
        console.log("selectedCompany:", selectedCompany)
    }

    useEffect(() => {
        console.log("selectedCompany useffect:", selectedCompany)
    }, [selectedCompany])

    const companyChanged = ({ key }) => {
        if (key === "createBranch") {
            history.push("/main/company/new")
        }
        else {
            setCompany(key);
        }
    }

    const loadingRoute = () => <Container><div style={{ display: 'flex', padding: 20, maxWidth: 620, borderRadius: 4, }}><div style={{ width: '100%' }}><Skeleton active avatar paragraph={{ rows: 4 }} /></div></div></Container>;

    const profile_content = (
        <div style={{ display: 'flex', flexDirection: 'column', padding: 16, alignItems: "start" }}>
            <Button type="link" style={{ color: Colors.lightBlue[800], fontWeight: 'bold', marginTop: 5, padding: "0 0 0 0" }} ghost
                onClick={signOut}>
                <div style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                    <LogoutOutlined style={{ fontSize: 16 }} />  <span style={{ marginLeft: 6 }}>Sign Out
                    </span>
                </div>
            </Button>
        </div>
    );

    const companyBranches = (
        <Menu onClick={companyChanged}>
            {branchList.length > 0 ? branchList.map(branch => {
                return <Menu.Item key={branch.id} icon={<CarOutlined />}>
                    {branch.title}
                </Menu.Item>
            }) : null
            }
            <Menu.Item key="createBranch" icon={<PlusCircleTwoTone />}>Create a branch</Menu.Item>
        </Menu>
    );

    return unauthenticated ? <Redirect to="/" /> :
        <Layout className="layout">
            <Content>
                <Header style={{
                    position: 'sticky', top: 0, zIndex: 10, paddingLeft: 0, paddingRight: 16, height: 64, display: 'flex', justifyContent: 'space-between', alignItems: 'center',
                    backgroundColor: Colors.white, width: '%100', borderBottom: "1px solid " + Colors.lightBlue[800]
                }}>
                    <div style={{ display: 'flex', width: '100%', justifyContent: 'space-between', height: "inherit" }}>
                        <div style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', backgroundColor: Colors.lightBlue[800], width: 200 }}>
                            <div onClick={goDashboard}
                                className="d-none d-md-block"
                                style={{ width: "max-content" }}>
                                <div style={{ display: 'flex', flexDirection: 'row', width: "max-content", alignItems: 'center', cursor: 'pointer' }}>
                                    <img alt="logo" src={require('../../Images/triparkLogoV2_fit_opacity.png')} height="45" />
                                </div>
                            </div>
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}>
                            <Dropdown overlay={companyBranches} trigger={['click']}>
                                <Button>
                                    {branchList.length > 0 && (selectedCompany && selectedCompany !== undefined) ? selectedCompany.title : "New Branch"} <DownOutlined />
                                </Button>
                            </Dropdown>
                        </div>
                        <div style={{ cursor: 'pointer', display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-around' }}>
                            <Popover
                                content={profile_content} placement={"bottomRight"} trigger="click">
                                <div style={{ height: 40, marginLeft: 6, paddingLeft: 4, paddingRight: 4, border: "1px solid " + Colors.gray[200], display: 'flex', alignItems: 'center', justifyContent: 'space-between', maxWidth: window.innerWidth > 768 ? 180 : 40, borderRadius: 20 }}>
                                    <Avatar icon={<UserOutlined />} style={{ backgroundColor: Colors.gray[300], display: 'flex', flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}></Avatar>
                                </div>
                            </Popover>
                        </div>
                    </div>
                </Header>
                {
                    CompanyService.branches.loading ?
                        <div style={{ padding: 20 }}><Skeleton active /></div>
                        :
                        <Layout style={{ height: "calc(100vh-100px)", width: "100%" }}>

                            <Sider trigger={null} collapsible collapsed={collapsed}>
                                <Menu theme="light" mode="inline" defaultSelectedKeys={['1']}
                                    style={{ height: '100%', backgroundColor: Colors.yellow }}>
                                    <div style={{ cursor: "pointer", display: "flex", flexDirection: "row", alignItems: "center", justifyContent: "center", padding: 6, backgroundColor: Colors.lightBlue[100] }} onClick={toggle}>
                                        {collapsed ? <RightSquareTwoTone style={{ fontSize: 18 }} /> : <LeftSquareTwoTone style={{ fontSize: 18 }} />}
                                    </div>
                                    {
                                        menu.map(menuItem => {
                                            if (menuItem.items && menuItem.items.length > 0) {
                                                return <SubMenu key={menuItem.key} icon={menuItem.icon} title={menuItem.defaultValue}>
                                                    {menuItem.items.map(subMenuItem => {
                                                        return <Menu.Item key={subMenuItem.key} icon={subMenuItem.icon} onClick={() => menuRedirect(subMenuItem.url)}>{subMenuItem.defaultValue}</Menu.Item>
                                                    })}
                                                </SubMenu>
                                            }
                                            else {
                                                return <Menu.Item key={menuItem.key} icon={menuItem.icon} onClick={() => menuRedirect(menuItem.url)}>
                                                    {menuItem.defaultValue}
                                                </Menu.Item>
                                            }
                                        })
                                    }
                                </Menu>
                            </Sider>

                            <Suspense fallback={loadingRoute()} >
                                <Switch>
                                    {routes.filter(c => c.authenticatedUser && c.allowedRoles.includes(Roles.WEB_CLIENT_ADMIN)).map((route, index) => {
                                        return route.component ? (
                                            <Route
                                                key={index}
                                                path={route.path}
                                                exact={route.exact}
                                                name={route.name}
                                                render={props => (
                                                    <div style={{ height: 'calc(100vh - 66px)', overflowX: 'hidden', paddingTop: route.marginTop || 24, paddingBottom: 16, paddingRight: 24, paddingLeft: 24, width: "100%" }}>
                                                        <route.component {...props} />
                                                    </div>
                                                )} />
                                        ) : (null);
                                    })}
                                </Switch>
                            </Suspense>
                        </Layout>
                }
            </Content>
        </Layout>
})

export default MainLayout;