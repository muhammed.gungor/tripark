import React from 'react';
import {Roles} from '../../enums/Roles';


const SignIn = React.lazy(() => import('../Pages/Authentication/SignIn'));
const Register = React.lazy(() => import('../Pages/Authentication/Register'));
const CompanyDashboard = React.lazy(() => import('../Pages/Company/Dashboard'));
const AdminCompanyList = React.lazy(() => import('../Pages/Admin/Company/List'));
const CompanyProfile = React.lazy(() => import('../Pages/Company/Profile'));
const AdminUserList = React.lazy(() => import('../Pages/Admin/UserManagement/List'));
const CompanyMapSettings = React.lazy(() => import('../Pages/Company/MapSettings'));
const AutoparkFields = React.lazy(() => import('../Pages/Company/AutoparkFields'));
const WorkingHours = React.lazy(() => import('../Pages/Company/WorkingHours'));

const TriparkRoutes = [
    { path: '/', exact: true, name: 'SignIn', component: SignIn, authenticatedUser:false },
    { path: '/signin', exact: true, name: 'SignIn', component: SignIn, authenticatedUser:false },
    { path: '/register', exact: true, name: 'Register', component: Register, authenticatedUser:false },
    { path: '/main/dashboard', exact: true, name: 'CompanyDashboard', component: CompanyDashboard, authenticatedUser:true,allowedRoles: [Roles.WEB_CLIENT_ADMIN] },
    { path: '/admin/company/list', exact: true, name: 'CompanyList', component: AdminCompanyList, authenticatedUser:true,allowedRoles: [Roles.SYSTEM_ADMIN] },
    { path: '/admin/user/list', exact: true, name: 'UserList', component: AdminUserList, authenticatedUser:true,allowedRoles: [Roles.SYSTEM_ADMIN] },
    { path: '/main/company/profile', exact: true, name: 'CompanyProfile', component: CompanyProfile, authenticatedUser:true,allowedRoles: [Roles.WEB_CLIENT_ADMIN] },
    { path: '/main/company/new', exact: true, name: 'CompanyProfileNew', component: CompanyProfile, authenticatedUser:true,allowedRoles: [Roles.WEB_CLIENT_ADMIN] },
    { path: '/main/company/map-settings', exact: true, name: 'CompanyProfileNew', component: CompanyMapSettings, authenticatedUser:true,allowedRoles: [Roles.WEB_CLIENT_ADMIN] },
    { path: '/main/company/autopark-fields', exact: true, name: 'AutoparkFields', component: AutoparkFields, authenticatedUser:true,allowedRoles: [Roles.WEB_CLIENT_ADMIN] },
    { path: '/main/company/working-hours', exact: true, name: 'WorkingHours', component: WorkingHours, authenticatedUser:true,allowedRoles: [Roles.WEB_CLIENT_ADMIN] }
]    

export default TriparkRoutes;