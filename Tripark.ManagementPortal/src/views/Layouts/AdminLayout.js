import React, { Suspense, useEffect, useState } from 'react';
import {
    Layout, Menu, Skeleton, Button, Avatar, Popover
} from 'antd';
import 'antd/dist/antd.css';
import { Redirect, Route, Switch } from 'react-router-dom';
import { useCookies } from 'react-cookie';
//import windowSize from 'react-window-size';
import { Container } from 'reactstrap';
import { observer } from 'mobx-react';
import CompanyService from '../../services/CompanyService';
import AuthenticationService from '../../services/AuthenticationService';
import { showMessage } from '../../commons/Helpers';
import Colors from '../../commons/Colors';
import { LogoutOutlined, UserOutlined, RightSquareTwoTone, LeftSquareTwoTone, BankTwoTone, SettingTwoTone } from '@ant-design/icons';
import { useHistory } from 'react-router-dom';
import ServiceBase from '../../services/ServiceBase';
import { HttpStatusCodes } from '../../settings/HttpUtils';
import routes from "../Layouts/Routes"
import { Roles } from '../../enums/Roles';

const { SubMenu } = Menu;
const { Header, Sider, Content } = Layout;

const menu = [
    {
        "languageKey": "MENU_COMPANIES",
        "icon": <BankTwoTone />,
        "key": "companies",
        "url": "company/list",
        "defaultValue": "Companies"
    },
    {
        "languageKey": "MENU_USERS",
        "icon": <UserOutlined twoToneColor={Colors.tripark} />,
        "key": "users",
        "url": "user/list",
        "defaultValue": "Users"
    },
    {
        "languageKey": "SETTINGS",
        "key": "settings",
        "icon": <SettingTwoTone />,
        "items": [
            {
                "languageKey": "CONFS",
                "key": "configurations",
                "defaultValue": "Configurations"
            }
        ],
        "defaultValue": "General Settings"
    }
];


const MainLayout = observer(() => {
    const [constructorHasRun, setConstructorHasRun] = useState(false);
    const [unauthenticated, setUnauthenticated] = useState(false);
    const [collapsed, setCollapsed] = useState(false);
    const [cookie, setCookie] = useCookies();
    const history = useHistory();

    const signOut = () => {
        setCookie("loginData", null, { path: "/" });
        history.push('/')
    }

    const constructor = () => {
        if (constructorHasRun) return;

        console.log("Admin Layout constructor")

        AuthenticationService.login = cookie["loginData"];

        console.log("Cookies", cookie, ServiceBase.response.statusCode)

        if (AuthenticationService.login === undefined || AuthenticationService.login.data === undefined || AuthenticationService.userToken === null || ServiceBase.response.statusCode === HttpStatusCodes.UNAUTHORIZED) {
            setUnauthenticated(true);
            console.log("SESSION EXPIRED.")
            showMessage("warning", "Your session has been expired. Please sign in again.")
            signOut();
        }
        else {
            setUnauthenticated(false);
        }

        setConstructorHasRun(true);
    };

    constructor();

    useEffect(() => {
        if (!unauthenticated) {
            console.log("Admin layout useEffect method. Authorized user here.")
        }
    }, [unauthenticated]);

    const toggle = () => {
        setCollapsed(!collapsed)
    };

    const goDashboard = () => {
        history.push('/admin')
    }

    const menuRedirect = (menuUrl) => {
        history.push('/admin/' + menuUrl);
    }

    const loadingRoute = () => <Container><div style={{ display: 'flex', padding: 20, maxWidth: 620, borderRadius: 4, }}><div style={{ width: '100%' }}><Skeleton active avatar paragraph={{ rows: 4 }} /></div></div></Container>;

    const profile_content = (
        <div style={{ display: 'flex', flexDirection: 'column', padding: 16, alignItems: "start" }}>
            <Button type="link" style={{ color: Colors.lightBlue[800], fontWeight: 'bold', marginTop: 5, padding: "0 0 0 0" }} ghost
                onClick={signOut}>
                <div style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                    <LogoutOutlined style={{ fontSize: 16 }} />  <span style={{ marginLeft: 6 }}>Sign Out
                    </span>
                </div>
            </Button>
        </div>
    );

    return unauthenticated ? <Redirect to="/" /> :
        <Layout className="layout">
            <Content>
                <Header style={{
                    position: 'sticky', top: 0, zIndex: 10, paddingLeft: 0, paddingRight: 16, height: 64, display: 'flex', justifyContent: 'space-between', alignItems: 'center',
                    backgroundColor: Colors.white, width: '%100', borderBottom: "1px solid " + Colors.lightBlue[800]
                }}>
                    <div style={{ display: 'flex', width: '100%', justifyContent: 'space-between', height: "inherit" }}>
                        <div style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', backgroundColor: Colors.lightBlue[800], width: 200 }}>
                            <div onClick={goDashboard}
                                className="d-none d-md-block"
                                style={{ width: "max-content" }}>
                                <div style={{ display: 'flex', flexDirection: 'row', width: "max-content", alignItems: 'center', cursor: 'pointer' }}>
                                    <img alt="logo" src={require('../../Images/triparkLogoV2_fit_opacity.png')} height="45" />
                                </div>
                            </div>
                        </div>

                        <div style={{ cursor: 'pointer', display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-around' }}>
                            <Popover
                                content={profile_content} placement={"bottomRight"} trigger="click">
                                <div style={{ height: 40, marginLeft: 6, paddingLeft: 4, paddingRight: 4, border: "1px solid " + Colors.gray[200], display: 'flex', alignItems: 'center', justifyContent: 'space-between', maxWidth: window.innerWidth > 768 ? 180 : 40, borderRadius: 20 }}>
                                    <Avatar icon={<UserOutlined />} style={{ backgroundColor: Colors.gray[300], display: 'flex', flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}></Avatar>
                                </div>
                            </Popover>
                        </div>
                    </div>
                </Header>
                {
                    CompanyService.branches.loading ?
                        <div style={{ padding: 20 }}><Skeleton active /></div>
                        :
                        <Layout style={{ height: "calc(100vh)" }}>

                            <Sider trigger={null} collapsible collapsed={collapsed}>
                                <Menu theme="light" mode="inline" defaultSelectedKeys={['1']}
                                    style={{ height: '100%', backgroundColor: Colors.yellow }}>
                                    <div style={{ cursor: "pointer", display: "flex", flexDirection: "row", alignItems: "center", justifyContent: "center", padding: 6, backgroundColor: Colors.lightBlue[100] }} onClick={toggle}>
                                        {collapsed ? <RightSquareTwoTone style={{ fontSize: 18 }} /> : <LeftSquareTwoTone style={{ fontSize: 18 }} />}
                                    </div>
                                    {
                                        menu.map(menuItem => {
                                            if (menuItem.items && menuItem.items.length > 0) {
                                                return <SubMenu key={menuItem.key} icon={menuItem.icon} title={menuItem.defaultValue}>
                                                    {menuItem.items.map(subMenuItem => {
                                                        return <Menu.Item key={subMenuItem.key}>{subMenuItem.defaultValue}</Menu.Item>
                                                    })}
                                                </SubMenu>
                                            }
                                            else {
                                                return <Menu.Item key={menuItem.key} icon={menuItem.icon} onClick={() => menuRedirect(menuItem.url)}>
                                                    {menuItem.defaultValue}
                                                </Menu.Item>
                                            }
                                        })
                                    }
                                </Menu>
                            </Sider>
                            <Content>
                                <Suspense fallback={loadingRoute()} >
                                    <Switch>
                                        {routes.filter(c => c.authenticatedUser && c.allowedRoles.includes(Roles.SYSTEM_ADMIN)).map((route, index) => {
                                            return route.component ? (
                                                <Route
                                                    key={index}
                                                    path={route.path}
                                                    exact={route.exact}
                                                    name={route.name}
                                                    render={props => (
                                                        <div style={{ height: 'calc(100vh - 66px)', padding:32 }}>
                                                            <route.component {...props} />
                                                        </div>
                                                    )} />
                                            ) : (null);
                                        })}

                                    </Switch>
                                </Suspense>
                            </Content>
                        </Layout>
                }
            </Content>
        </Layout>
})

export default MainLayout;